#!/bin/bash
cd ..

gpu=$1
dataset=ag_news
#dataset=yahoo_answers
#dataset=dbpedia
#dataset=yelp_review_full
#dataset=yelp_review_polarity
#dataset=amazon_review_full
#dataset=amazon_review_polarity
save_root=~/pbase/x/p704/results/temp//

max_iter=5000
optimizer=lazy_adam
learning_rate=1e-3
batch_size=256
batch_size=64
dims=300
reg_weight=0
concat_maxpooling=True
ec_logits_bn=1.
use_recoding=True
ec_code_dropout=0.
ec_STE_softmax_transform=True
ec_shared_coding=False
ec_hard_code_output=True

for dataset in ag_news yahoo_answers dbpedia yelp_review_full yelp_review_polarity amazon_review_full amazon_review_polarity; do
for KD in 32,32 ;do
#for KD in 32,32 16,32 32,16 16,16;do
IFS=","; set -- $KD; K=$1; D=$2;
for ec_code_emb_dim in 300; do
#for ec_code_emb_dim in 5 10 50; do
for ec_aggregator in mean; do
#for ec_aggregator in mean mean_fnn rnn; do
for ec_code_generator in gumbel_softmax ; do
#for ec_code_generator in STE_argmax gumbel_softmax preassign; do
for ec_emb_baseline in False ; do
#for ec_emb_baseline in False True; do
for ec_emb_baseline_reg in 0; do
#for ec_emb_baseline_reg in 0.1 1 10 100 1000;do
for ec_emb_baseline_dropout in 0. ; do
for ec_entropy_reg in 0.1; do
#for ec_entropy_reg in 0.1 1; do
	data_dir=~/dbase/corpus/text_classification_char_cnn/$dataset\_csv/
	emb_load_filename=~/pbase/x/p704/pretrains/text_classification/$dataset\_embs.pkl
	#emb_load_filename=
	#ec_temperature_decay_method=inv_time
	ec_temperature_decay_method=none
	ec_temperature_decay_steps=100
	ec_temperature_decay_rate=1.
	ec_temperature_init=1.
	ec_temperature_lower_bound=1e-7

	save_path=$save_root/$(date +"%m%d_%H%M%S")\_$dataset\_$ec_code_generator\_K$K\_D$D\_dim$dims\_cdim$ec_code_emb_dim\_gator$ec_aggregator\_hard$ec_hard_code_output\_shared$ec_shared_coding\_baseline$ec_emb_baseline\_basereg$ec_emb_baseline_reg\_basedrop$ec_emb_baseline_dropout\_entreg$ec_entropy_reg
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python main.py --dataset=$dataset --data_dir=$data_dir --save_dir=$save_path --optimizer=$optimizer --batch_size=$batch_size --learning_rate=$learning_rate --reg_weight=$reg_weight --dims=$dims --concat_maxpooling=$concat_maxpooling --use_recoding=$use_recoding --K=$K --D=$D --ec_code_emb_dim=$ec_code_emb_dim --ec_code_generator=$ec_code_generator --ec_aggregator=$ec_aggregator --ec_hard_code_output=$ec_hard_code_output --ec_logits_bn=$ec_logits_bn --ec_temperature_decay_method=$ec_temperature_decay_method --ec_STE_softmax_transform=$ec_STE_softmax_transform --ec_shared_coding=$ec_shared_coding --ec_temperature_decay_steps=$ec_temperature_decay_steps --ec_temperature_decay_rate=$ec_temperature_decay_rate --ec_temperature_init=$ec_temperature_init --ec_temperature_lower_bound=$ec_temperature_lower_bound --ec_code_dropout=$ec_code_dropout --ec_emb_baseline=$ec_emb_baseline --ec_emb_baseline_reg=$ec_emb_baseline_reg --max_iter=$max_iter --ec_entropy_reg=$ec_entropy_reg --emb_load_filename=$emb_load_filename --ec_emb_baseline_dropout=$ec_emb_baseline_dropout >$save_path/log 2>&1 #&
	#let "gpu+=1"
done
done
done
done
done
done
done
done
done

# STE_argmax using ec_entropy_reg=1.
# gumbel_softmax using ec_entropy_reg=0.1
# base_reg should be around 1

