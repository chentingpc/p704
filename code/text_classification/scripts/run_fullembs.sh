#!/bin/bash
############################################################################
# This script is used to generate full-embedding and low-rank benchmarks.
# Options:
#   dataset: different datasets to test on.
#   emb_lowrank_dim: ratio of embedding parameters to keep when in (0, 1).
#   save_embedding: whether or not to save the word embedding matrix.
#   concat_maxpooling: whether or not to use maxpooling with average pooling
#     by concatenation.
#   word_emb_batchnorm: whether or not to use batch normalization on word
#     embedding vectors of sentences.
# Any other options should be kept as default.
############################################################################
cd ..
gpu=$1
echo $gpu

dataset=ag_news
#dataset=yahoo_answers
#dataset=dbpedia
#dataset=yelp_review_full
#dataset=yelp_review_polarity
#dataset=amazon_review_full
#dataset=amazon_review_polarity
save_embedding=false

save_root=~/pbase/x/p704/results/temp/
use_recoding=False
max_iter=5000
optimizer=lazy_adam
learning_rate=1e-3
dims=300
reg_weight=0

for dataset in ag_news yahoo_answers dbpedia yelp_review_full yelp_review_polarity amazon_review_full amazon_review_polarity; do
for batch_size in 256 ; do
#for batch_size in 512 256; do  # 512 seems better
for dims in 300 ; do
#for dims in 300 500 1000; do  # 500 may be optimal
for concat_maxpooling in True ; do  # True better
#for concat_maxpooling in True False; do  # True better
for word_emb_batchnorm in False; do  # False better
#for word_emb_batchnorm in True False; do  # False better
for emb_lowrank_dim in 0.0 ; do
#for emb_lowrank_dim in 0.01 0.05 0.1 0.2; do
	echo $dataset
	data_dir=~/dbase/corpus/text_classification_char_cnn/$dataset\_csv/
	if [ $save_embedding == true ]; then
		emb_save_filename=~/pbase/x/p704/pretrains/text_classification/$dataset\_embs.pkl
		echo "Will save embedding to $emb_save_filename"
	else
		echo "Won't save embedding"
	fi

	if [ $dataset == ag_news ]; then
		max_iter=1000
	elif [ $dataset == yahoo_answers ]; then
		max_iter=3500
	elif [ $dataset == dbpedia ]; then
		max_iter=1500
	elif [ $dataset == yelp_review_full ]; then
		max_iter=2000
	elif [ $dataset == yelp_review_polarity ]; then
		max_iter=1500
	fi

	save_path=$save_root/$(date +"%m%d_%H%M%S")\_$dataset\_bsize$batch_size\_dims$dims\_cmax$concat_maxpooling\_wembbn$word_emb_batchnorm\_lowrankd$emb_lowrank_dim
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python main.py --dataset=$dataset --data_dir=$data_dir --save_dir=$save_path --optimizer=$optimizer --batch_size=$batch_size --learning_rate=$learning_rate --reg_weight=$reg_weight --dims=$dims --use_recoding=$use_recoding --max_iter=$max_iter --concat_maxpooling=$concat_maxpooling --word_emb_batchnorm=$word_emb_batchnorm --emb_save_filename=$emb_save_filename --emb_lowrank_dim=$emb_lowrank_dim >$save_path/log 2>&1
done
done
done
done
done
done
