import sys
import numpy as np
import tensorflow as tf

sys.path.insert(0, "../lm")
import util
from encoder import Encoder


class Model(object):
  def __init__(self, hparams, emb_load_filename=None, code_load_filename=None):
    self._hparams = hparams
    self._use_recoding = hparams.use_recoding

    # Load pretrained embedding.
    if hparams.ec_emb_baseline and (
        emb_load_filename is not None and emb_load_filename != ""):
      try:
        # pickle format.
        with open(emb_load_filename) as fp:
          self._pretrained_emb = pickle.load(fp)
      except:
        # npz format.
        self._pretrained_emb = np.load(emb_load_filename + ".npz")["emb"]
    else:
      self._pretrained_emb = None
    if hparams.use_recoding:
      with tf.variable_scope("disco_embeddings", reuse=False):
        self._encoder = Encoder(K=hparams.K,
                                D=hparams.D,
                                d=hparams.ec_code_emb_dim,
                                outd=hparams.dims,
                                hparams=hparams,
                                vocab_size=hparams.vocab.shape[0],
                                code_type=hparams.code_type,
                                code_emb_initializer=None,
                                code_initializer=code_load_filename,
                                emb_baseline=hparams.ec_emb_baseline,
                                create_code_logits=not hparams.ec_emb_autoencoding,
                                pretrained_emb=self._pretrained_emb)

      if hparams.ec_code_generator == "preassign":
        self._ec_is_one_hot = False
      else:
        self._ec_is_one_hot = True

      if hparams.ec_code_generator == "pg":
        self._pg_first_n4f = hparams.pg_num_samples
      elif hparams.ec_code_generator == "relax":
        self._pg_first_n4f = 1

  def forward(self, features, labels, is_training=False):
    """Returns loss, preds, train_op.
    
    Args:
      features: (batch_size, max_seq_length)
      labels: (batch_size, num_classes)

    Returns:
      loss: (batch_size, )
      preds: (batch_size, )
      train_op: op.
    """
    hparams = self._hparams
    emb_dims = hparams.dims
    learning_rate = hparams.learning_rate
    reg_weight = hparams.reg_weight
    max_grad_norm = hparams.max_grad_norm
    vocab = hparams.vocab
    num_words = len(vocab)
    num_classes = labels.shape.as_list()[-1]
    batch_size = tf.shape(features)[0]

    lengths = tf.reduce_sum(  # (batch_size, 1) of lengths of features
        tf.cast(tf.greater(features, 0), tf.float32), axis=1, keep_dims=True)
    if not self._use_recoding:
      with tf.variable_scope("embeddings", reuse=not is_training):
        # Get embedding matrix and indexing out the sentence word embeddings.
        if hparams.emb_lowrank_dim == 0:
          def _get_word_emb(device="gpu"):
            with tf.device("/%s:0" % device):
              if self._pretrained_emb is None:
                word_embedding = tf.get_variable(
                    "word_embedding", [num_words, emb_dims], dtype=tf.float32)
              else:
                word_embedding = tf.get_variable(
                    "word_embedding",
                    initializer=self._pretrained_emb, trainable=False)
              return word_embedding
          word_embedding = _get_word_emb()
          # Using CPU to hold word embedding due to GPU memory limit, much slower.
          # word_embedding = _get_word_emb(device="cpu")
          embs = tf.nn.embedding_lookup(word_embedding, features)  # (batch_size, max_seq_length, dim)
        else:
          # Low rank approximate the embedding matrix.
          _dim = hparams.emb_lowrank_dim
          if _dim < 1.:  # Represents ratio of low-rank parameters to full one.
            _dim = int(_dim * num_words / (num_words + emb_dims) * emb_dims)
          embedding_p = tf.get_variable(
              "embedding_p", [num_words, _dim], dtype=tf.float32)
          embedding_q = tf.get_variable(
              "embedding_q", [_dim, emb_dims], dtype=tf.float32)
          embs = tf.nn.embedding_lookup(embedding_p, features)
          embs = tf.tensordot(embs, embedding_q, [[-1], [0]])
        if hparams.word_emb_batchnorm:
          embs = tf.layers.batch_normalization(embs, training=is_training)
    else:
      with tf.variable_scope("disco_embeddings", reuse=not is_training):
        if hparams.ec_emb_baseline:
          code_logits = None
          if hparams.ec_emb_autoencoding:
            _embs = tf.nn.embedding_lookup(self._pretrained_emb, features)
            code_logits = self._encoder.embed_transpose(
                _embs, is_training=is_training)
          codes, code_embs, embsb = self._encoder.symbol2code(
              features, logits=code_logits,
              is_training=is_training, output_embb=True)
        else:
          embsb = None
          codes, code_embs = self._encoder.symbol2code(
              features, is_training=is_training)
        embs = self._encoder.embed(
            codes, code_embs=code_embs, embsb=embsb,
            is_one_hot=self._ec_is_one_hot, is_training=is_training)
    word_embs = embs

    with tf.variable_scope("transform", reuse=not is_training):
      if hparams.ec_code_generator in ["pg", "relax"]:
        lengths = tf.tile(lengths, [hparams.pg_num_samples, 1])
        labels = tf.tile(labels, [hparams.pg_num_samples, 1])
        embs = tf.reshape(embs, [-1, features.shape.as_list()[1], emb_dims])
      embs_meanpool = tf.reduce_sum(embs, -2) / lengths  # Mean pooling.
      embs_maxpool = tf.reduce_max(embs, 1)  # Max pooling.
      if hparams.concat_maxpooling:
        embs = tf.concat([embs_meanpool, embs_maxpool], -1)
      else:
        embs = embs_meanpool
      # embs = tf.layers.batch_normalization(embs, training=is_training)
      logits = tf.layers.dense(embs, num_classes)
      preds = tf.argmax(logits, -1)[:batch_size]
      loss = tf.nn.softmax_cross_entropy_with_logits(
          labels=labels, logits=logits)

    if is_training:
      # Add policy loss updater.
      if hparams.use_recoding and hparams.ec_code_generator in ["pg", "relax"]:
        policy_theta = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES,
            scope="model/disco_embeddings/code/code_logits")
        if len(policy_theta) != 1:
          raise ValueError("{}".format(policy_theta))
        policy_theta += tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES,
            scope="model/disco_embeddings/symbol2code")
        tvars = tf.trainable_variables()
        non_pg_tvars = []
        for tvar in tvars:
          if not tvar.name.startswith("model/disco_embeddings/code"):
            non_pg_tvars.append(tvar)
        if len(tvars) == len(non_pg_tvars):
          raise ValueError("")
        loss = tf.reshape(loss, [-1, batch_size])
        with tf.variable_scope("disco_embeddings"):
          train_op_policy = self._encoder._pgor.backward(
              loss, policy_theta)
        loss = tf.reshape(loss[:self._pg_first_n4f], [-1])
      else:
        non_pg_tvars = tf.trainable_variables()
        train_op_policy = None

      # Regular loss updater.
      #learning_rate = tf.train.inverse_time_decay(
      #    learning_rate, tf.train.get_or_create_global_step(),
      #    decay_steps=1000, decay_rate=0.5, staircase=True)
      loss_scalar = tf.reduce_mean(loss)
      loss_scalar += reg_weight * tf.reduce_mean(word_embs**2)
      loss_scalar += tf.reduce_sum(
          tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
      train_op = tf.contrib.layers.optimize_loss(
          loss=loss_scalar,
          global_step=tf.train.get_or_create_global_step(),
          learning_rate=learning_rate,
          optimizer=util.get_optimizer(hparams.optimizer),
          variables=non_pg_tvars,
          clip_gradients=max_grad_norm,
          summaries=["learning_rate", "loss", "global_gradient_norm"])
      if train_op_policy is not None:
        train_op = tf.group(train_op, train_op_policy)
    else:
      train_op = False
      loss_scalar = None
      if hparams.use_recoding and hparams.ec_code_generator in ["pg", "relax"]:
        loss = tf.reshape(loss, [-1, batch_size])
        loss = tf.reshape(loss[:self._pg_first_n4f], [-1])

    return loss_scalar, preds, train_op
