import os
import sys
import time
import numpy as np
import tensorflow as tf
import cPickle as pickle
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # TODO: avoid the OutOfRangeError msg.

import data
from model import Model
sys.path.insert(0, "../lm")
import util

flags = tf.flags
FLAGS = flags.FLAGS
flags.DEFINE_string("dataset", None, "")
flags.DEFINE_string("data_dir", None, "")
flags.DEFINE_string("save_dir", None, "")
flags.DEFINE_integer("max_iter", 10000, "")
flags.DEFINE_integer("eval_every_num_iter", 100, "")
flags.DEFINE_integer("batch_size", 1024, "")
flags.DEFINE_string("optimizer", "sgd", "")
flags.DEFINE_float("learning_rate", 1e-3, "")
flags.DEFINE_float("max_grad_norm", 10., "")
flags.DEFINE_integer("dims", 100, "")
flags.DEFINE_bool("concat_maxpooling", False,
                  "Whether or not to use concat(mean_pooling, maxpooling).")
flags.DEFINE_bool("word_emb_batchnorm", False,
                  "Whether or not to use batch_norm for aggregated word embs.")
flags.DEFINE_float("reg_weight", 0, "")
flags.DEFINE_float("emb_lowrank_dim", 0, "")
flags.DEFINE_bool("test_summary", False, "")

# Code related
flags.DEFINE_bool("use_recoding", False, "Whether or not to use KD code.")
flags.DEFINE_integer("D", 10, "D-dimensional code.")
flags.DEFINE_integer("K", 100, "K-way code.")
flags.DEFINE_string("code_type", "redundant", "Type of the code.")
flags.DEFINE_string("code_save_filename", None, "")
flags.DEFINE_string("code_load_filename", None, "")
flags.DEFINE_string("emb_save_filename", None, "")
flags.DEFINE_string("emb_load_filename", None, "")
flags.DEFINE_string("ec_code_generator", "STE_argmax", "")
flags.DEFINE_bool("ec_emb_baseline", False, "")
flags.DEFINE_float("ec_emb_baseline_reg", 0., "")
flags.DEFINE_float("ec_emb_baseline_dropout", 0., "")
flags.DEFINE_float("ec_logits_bn", 0, "")
flags.DEFINE_float("ec_entropy_reg", 0., "")
flags.DEFINE_string("ec_temperature_decay_method", "none", "exp, inv_time")
flags.DEFINE_integer("ec_temperature_decay_steps", 500, "")
flags.DEFINE_float("ec_temperature_decay_rate", 1, "")
flags.DEFINE_float("ec_temperature_init", 1., "")
flags.DEFINE_float("ec_temperature_lower_bound", 1e-5, "")
flags.DEFINE_bool("ec_shared_coding", False, "")
flags.DEFINE_float("ec_code_dropout", None, "")
flags.DEFINE_bool("ec_STE_softmax_transform", True, "")
flags.DEFINE_bool("ec_hard_code_output", True, "")
flags.DEFINE_integer("ec_code_emb_dim", 100, "")
flags.DEFINE_string("ec_aggregator", "mean_fnn", "")
flags.DEFINE_integer("ec_fnn_num_layers", 1, "")
flags.DEFINE_integer("ec_fnn_hidden_size", 500, "")
flags.DEFINE_string("ec_fnn_hidden_actv", "relu", "")
flags.DEFINE_integer("ec_cnn_num_layers", 1, "")
flags.DEFINE_bool("ec_cnn_residual", True, "")
flags.DEFINE_string("ec_cnn_pooling", "max", "")
flags.DEFINE_integer("ec_cnn_filters", 100, "")
flags.DEFINE_integer("ec_cnn_kernel_size", 3, "")
flags.DEFINE_string("ec_cnn_hidden_actv", "relu", "")
flags.DEFINE_integer("ec_rnn_num_layers", 1, "")
flags.DEFINE_integer("ec_rnn_hidden_size", 100, "")
flags.DEFINE_bool("ec_rnn_additive_pooling", "tanh", "")
flags.DEFINE_bool("ec_rnn_residual", False, "")
flags.DEFINE_float("ec_rnn_dropout", 0., "")
flags.DEFINE_bool("ec_rnn_trainable_init_state", True, "")
flags.DEFINE_bool("ec_rnn_bidirection", False, "")
flags.DEFINE_float("ec_vq_reg_weight", 1., "")
flags.DEFINE_bool("ec_emb_autoencoding", False, "")
flags.DEFINE_integer("ec_emb_transpose_layers", 1, "")
flags.DEFINE_integer("ec_emb_transpose_dim", 100, "")
flags.DEFINE_string("ec_emb_transpose_actv", "tanh", "")

flags.DEFINE_string("policy_optimizer", "adam", "")
flags.DEFINE_float("policy_learning_rate", 1e-3, "")
flags.DEFINE_float("policy_var_learning_rate", 0., "")
flags.DEFINE_bool("policy_control_variate", True, "")
flags.DEFINE_integer("pg_num_samples", 2, "Number of samples for PG.")

# Setup hyper-parameters.
hparams = tf.contrib.training.HParams()
hparams.add_hparam("optimizer", FLAGS.optimizer)
hparams.add_hparam("learning_rate", FLAGS.learning_rate)
hparams.add_hparam("max_grad_norm", FLAGS.max_grad_norm)
hparams.add_hparam("dims", FLAGS.dims)
hparams.add_hparam("concat_maxpooling", FLAGS.concat_maxpooling)
hparams.add_hparam("word_emb_batchnorm", FLAGS.word_emb_batchnorm)
hparams.add_hparam("reg_weight", FLAGS.reg_weight)
hparams.add_hparam("emb_lowrank_dim", FLAGS.emb_lowrank_dim)
hparams.add_hparam("test_summary", FLAGS.test_summary)
# Code related.
hparams.add_hparam("use_recoding", FLAGS.use_recoding)
hparams.add_hparam("D", FLAGS.D)
hparams.add_hparam("K", FLAGS.K)
hparams.add_hparam("code_type", FLAGS.code_type)
hparams.add_hparam("code_save_filename", FLAGS.code_save_filename)
hparams.add_hparam("code_load_filename", FLAGS.code_load_filename)
hparams.add_hparam("emb_save_filename", FLAGS.emb_save_filename)
hparams.add_hparam("emb_load_filename", FLAGS.emb_load_filename)
hparams.add_hparam("ec_code_generator", FLAGS.ec_code_generator)
hparams.add_hparam("ec_emb_baseline", FLAGS.ec_emb_baseline)
hparams.add_hparam("ec_emb_baseline_reg", FLAGS.ec_emb_baseline_reg)
hparams.add_hparam("ec_emb_baseline_dropout", FLAGS.ec_emb_baseline_dropout)
hparams.add_hparam("ec_logits_bn", FLAGS.ec_logits_bn)
hparams.add_hparam("ec_entropy_reg", FLAGS.ec_entropy_reg)
hparams.add_hparam("ec_temperature_decay_method", FLAGS.ec_temperature_decay_method)
hparams.add_hparam("ec_temperature_decay_steps", FLAGS.ec_temperature_decay_steps)
hparams.add_hparam("ec_temperature_decay_rate", FLAGS.ec_temperature_decay_rate)
hparams.add_hparam("ec_temperature_init", FLAGS.ec_temperature_init)
hparams.add_hparam("ec_temperature_lower_bound", FLAGS.ec_temperature_lower_bound)
hparams.add_hparam("ec_shared_coding", FLAGS.ec_shared_coding)
hparams.add_hparam("ec_code_dropout", FLAGS.ec_code_dropout)
hparams.add_hparam("ec_STE_softmax_transform", FLAGS.ec_STE_softmax_transform)
hparams.add_hparam("ec_hard_code_output", FLAGS.ec_hard_code_output)
hparams.add_hparam("ec_code_emb_dim", FLAGS.ec_code_emb_dim)
hparams.add_hparam("ec_aggregator", FLAGS.ec_aggregator)
hparams.add_hparam("ec_fnn_num_layers", FLAGS.ec_fnn_num_layers)
hparams.add_hparam("ec_fnn_hidden_size", FLAGS.ec_fnn_hidden_size)
hparams.add_hparam("ec_fnn_hidden_actv", FLAGS.ec_fnn_hidden_actv)
hparams.add_hparam("ec_cnn_num_layers", FLAGS.ec_cnn_num_layers)
hparams.add_hparam("ec_cnn_residual", FLAGS.ec_cnn_residual)
hparams.add_hparam("ec_cnn_pooling", FLAGS.ec_cnn_pooling)
hparams.add_hparam("ec_cnn_filters", FLAGS.ec_cnn_filters)
hparams.add_hparam("ec_cnn_kernel_size", FLAGS.ec_cnn_kernel_size)
hparams.add_hparam("ec_cnn_hidden_actv", FLAGS.ec_cnn_hidden_actv)
hparams.add_hparam("ec_rnn_num_layers", FLAGS.ec_rnn_num_layers)
hparams.add_hparam("ec_rnn_hidden_size", FLAGS.ec_rnn_hidden_size)
hparams.add_hparam("ec_rnn_additive_pooling", FLAGS.ec_rnn_additive_pooling)
hparams.add_hparam("ec_rnn_residual", FLAGS.ec_rnn_residual)
hparams.add_hparam("ec_rnn_dropout", FLAGS.ec_rnn_dropout)
hparams.add_hparam("ec_rnn_trainable_init_state", FLAGS.ec_rnn_trainable_init_state)
hparams.add_hparam("ec_rnn_bidirection", FLAGS.ec_rnn_bidirection)
hparams.add_hparam("ec_vq_reg_weight", FLAGS.ec_vq_reg_weight)
hparams.add_hparam("ec_emb_autoencoding", FLAGS.ec_emb_autoencoding)
hparams.add_hparam("ec_emb_transpose_layers", FLAGS.ec_emb_transpose_layers)
hparams.add_hparam("ec_emb_transpose_dim", FLAGS.ec_emb_transpose_dim)
hparams.add_hparam("ec_emb_transpose_actv", FLAGS.ec_emb_transpose_actv)

hparams.add_hparam("policy_optimizer", FLAGS.policy_optimizer)
hparams.add_hparam("policy_learning_rate", FLAGS.policy_learning_rate)
hparams.add_hparam("policy_var_learning_rate", FLAGS.policy_var_learning_rate)
hparams.add_hparam("policy_control_variate", FLAGS.policy_control_variate)
hparams.add_hparam("pg_num_samples", FLAGS.pg_num_samples)


def main(_):
  (X_train_d, X_test_d, X_train_holder, X_test_holder, X_train, y_train, 
      X_test, y_test, test_reinitializer, vocab) = data.get_data(
      FLAGS.dataset, FLAGS.data_dir, FLAGS.batch_size)
  hparams.add_hparam("vocab", vocab)

  with tf.name_scope("Train"):
    with tf.variable_scope("model", reuse=False):
      m = Model(hparams, hparams.emb_load_filename, hparams.code_load_filename)
      loss_train, preds_train, train_op = m.forward(
          X_train, y_train, is_training=True)
  with tf.name_scope("Test"):
    with tf.variable_scope("model", reuse=True):
      loss_test, preds_test, _ = m.forward(
          X_test, y_test, is_training=False)

  # Verbose.
  print(hparams.values())
  print("Number of trainable params:    {}".format(
      util.get_parameter_count(
          excludings=["code_logits", "embb", "symbol2code"])))
  print(tf.trainable_variables())
  
  # Training session.
  init_feed_dict = {X_train_holder: X_train_d, X_test_holder: X_test_d}
  #sv = tf.train.Supervisor(logdir=FLAGS.save_dir, save_summaries_secs=5,
  #                         saver=None, init_feed_dict=init_feed_dict)
  sv = tf.train.Supervisor(saver=None, init_feed_dict=init_feed_dict)  # DEBUG
  config_proto = tf.ConfigProto(allow_soft_placement=True)
  config_proto.gpu_options.allow_growth = True
  with sv.managed_session(config=config_proto) as sess:
    del X_train_d, X_test_d
    print("Start training")
    # Training loop.
    losses_train_ = []
    start = time.time()
    for it in range(FLAGS.max_iter):
      if FLAGS.test_summary:
        sess.run(test_reinitializer)
      loss_train_, _ = sess.run([loss_train, train_op])
      losses_train_.append(loss_train_)

      # Evaluation.
      if it % FLAGS.eval_every_num_iter == 0:
        sess.run(test_reinitializer)
        train_hits = []
        test_hits = []
        for _ in range(100):
          results = sess.run([y_train, preds_train])
          train_hits.append(np.argmax(results[0], 1) == results[1])
        train_accuracy = np.concatenate(train_hits).mean()
        while True:
          try:
            results = sess.run([y_test, preds_test])
            test_hits.append(np.argmax(results[0], 1) == results[1])
          except tf.errors.OutOfRangeError:
            break
        test_accuracy = np.concatenate(test_hits).mean()
        end = time.time()
        print("Iter {:6}, {:.3} (secs), loss {:.4}, train acc {:.4} test acc {:.4}".format(
            it, end - start, np.mean(losses_train_), train_accuracy, test_accuracy))
        losses_train_ = []
        start = time.time()
    print("Final loss {:.4}, train acc {:.4} test acc {:.4}".format(
        np.mean(losses_train_), train_accuracy, test_accuracy))

    # Save embeddings.
    if FLAGS.emb_save_filename is not None and FLAGS.emb_save_filename != "":
      embs = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
                               scope="model/embeddings/word_embedding")
      if len(embs) != 1:
        raise ValueError("embs are {}".format(embs))
      embs = sess.run(embs)
      use_pkl = False
      if use_pkl:
        with open(FLAGS.emb_save_filename, "wb") as fp:  # Takes too much space.
          pickle.dump(embs[0].astype(np.float32), fp, 2)
      else:
        with open(FLAGS.emb_save_filename + ".npz", "w") as fp:
          np.savez_compressed(fp, emb=embs[0].astype(np.float32))

if __name__ == "__main__":
  tf.app.run()