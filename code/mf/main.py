import numpy as np
import cPickle as pickle
import tensorflow as tf
import sys
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # TODO: avoid the OutOfRangeError msg.

import data
from model import Model
sys.path.insert(0, "../lm")
import util

flags = tf.flags
FLAGS = flags.FLAGS
flags.DEFINE_string("dataset", None, "")
flags.DEFINE_string("save_dir", None, "")
flags.DEFINE_integer("max_iter", 2000, "")
flags.DEFINE_integer("eval_every_num_iter", 100, "")
flags.DEFINE_integer("batch_size", 1024, "")
flags.DEFINE_string("optimizer", "sgd", "")
flags.DEFINE_float("learning_rate", 1e-3, "")
flags.DEFINE_float("max_grad_norm", 10., "")
flags.DEFINE_integer("dims", 100, "")
flags.DEFINE_float("reg_weight", 0, "")
flags.DEFINE_float("emb_lowrank_dim", 0, "")
flags.DEFINE_bool("test_summary", False, "")

# Code related
flags.DEFINE_bool("use_recoding", False, "Whether or not to use KD code.")
flags.DEFINE_integer("D", 10, "D-dimensional code.")
flags.DEFINE_integer("K", 100, "K-way code.")
flags.DEFINE_string("code_type", "redundant", "Type of the code.")
flags.DEFINE_string("code_save_filename", None, "")
flags.DEFINE_string("code_load_filename", None, "")
flags.DEFINE_string("emb_save_filename", None, "")
flags.DEFINE_string("emb_load_filename", None, "")
flags.DEFINE_string("ec_code_generator", "STE_argmax", "")
flags.DEFINE_bool("ec_emb_baseline", False, "")
flags.DEFINE_float("ec_emb_baseline_reg", 0., "")
flags.DEFINE_float("ec_emb_baseline_dropout", 0., "")
flags.DEFINE_float("ec_logits_bn", 0, "")
flags.DEFINE_float("ec_entropy_reg", 0., "")
flags.DEFINE_string("ec_temperature_decay_method", "none", "exp, inv_time")
flags.DEFINE_integer("ec_temperature_decay_steps", 500, "")
flags.DEFINE_float("ec_temperature_decay_rate", 1, "")
flags.DEFINE_float("ec_temperature_init", 1., "")
flags.DEFINE_float("ec_temperature_lower_bound", 1e-5, "")
flags.DEFINE_bool("ec_shared_coding", False, "")
flags.DEFINE_float("ec_code_dropout", None, "")
flags.DEFINE_bool("ec_STE_softmax_transform", True, "")
flags.DEFINE_bool("ec_hard_code_output", True, "")
flags.DEFINE_integer("ec_code_emb_dim", 100, "")
flags.DEFINE_string("ec_aggregator", "mean_fnn", "")
flags.DEFINE_integer("ec_fnn_num_layers", 1, "")
flags.DEFINE_integer("ec_fnn_hidden_size", 100, "")
flags.DEFINE_string("ec_fnn_hidden_actv", "tanh", "")
flags.DEFINE_integer("ec_cnn_num_layers", 1, "")
flags.DEFINE_bool("ec_cnn_residual", True, "")
flags.DEFINE_string("ec_cnn_pooling", "max", "")
flags.DEFINE_integer("ec_cnn_filters", 100, "")
flags.DEFINE_integer("ec_cnn_kernel_size", 3, "")
flags.DEFINE_string("ec_cnn_hidden_actv", "tanh", "")
flags.DEFINE_integer("ec_rnn_num_layers", 1, "")
flags.DEFINE_integer("ec_rnn_hidden_size", 100, "")
flags.DEFINE_bool("ec_rnn_additive_pooling", "tanh", "")
flags.DEFINE_bool("ec_rnn_residual", False, "")
flags.DEFINE_float("ec_rnn_dropout", 0., "")
flags.DEFINE_bool("ec_rnn_trainable_init_state", True, "")
flags.DEFINE_bool("ec_rnn_bidirection", False, "")
flags.DEFINE_float("ec_vq_reg_weight", 1., "")
flags.DEFINE_bool("ec_emb_autoencoding", False, "")
flags.DEFINE_integer("ec_emb_transpose_layers", 1, "")
flags.DEFINE_integer("ec_emb_transpose_dim", 100, "")
flags.DEFINE_string("ec_emb_transpose_actv", "tanh", "")

flags.DEFINE_string("policy_optimizer", "adam", "")
flags.DEFINE_float("policy_learning_rate", 1e-3, "")
flags.DEFINE_float("policy_var_learning_rate", 0., "")
flags.DEFINE_bool("policy_control_variate", True, "")
flags.DEFINE_integer("pg_num_samples", 2, "Number of samples for PG.")

# Setup hyper-parameters.
hparams = tf.contrib.training.HParams()
hparams.add_hparam("optimizer", FLAGS.optimizer)
hparams.add_hparam("learning_rate", FLAGS.learning_rate)
hparams.add_hparam("max_grad_norm", FLAGS.max_grad_norm)
hparams.add_hparam("dims", FLAGS.dims)
hparams.add_hparam("reg_weight", FLAGS.reg_weight)
hparams.add_hparam("emb_lowrank_dim", FLAGS.emb_lowrank_dim)
hparams.add_hparam("test_summary", FLAGS.test_summary)
# Code related.
hparams.add_hparam("use_recoding", FLAGS.use_recoding)
hparams.add_hparam("D", FLAGS.D)
hparams.add_hparam("K", FLAGS.K)
hparams.add_hparam("code_type", FLAGS.code_type)
hparams.add_hparam("code_save_filename", FLAGS.code_save_filename)
hparams.add_hparam("code_load_filename", FLAGS.code_load_filename)
hparams.add_hparam("emb_save_filename", FLAGS.emb_save_filename)
hparams.add_hparam("emb_load_filename", FLAGS.emb_load_filename)
hparams.add_hparam("ec_code_generator", FLAGS.ec_code_generator)
hparams.add_hparam("ec_emb_baseline", FLAGS.ec_emb_baseline)
hparams.add_hparam("ec_emb_baseline_reg", FLAGS.ec_emb_baseline_reg)
hparams.add_hparam("ec_emb_baseline_dropout", FLAGS.ec_emb_baseline_dropout)
hparams.add_hparam("ec_logits_bn", FLAGS.ec_logits_bn)
hparams.add_hparam("ec_entropy_reg", FLAGS.ec_entropy_reg)
hparams.add_hparam("ec_temperature_decay_method", FLAGS.ec_temperature_decay_method)
hparams.add_hparam("ec_temperature_decay_steps", FLAGS.ec_temperature_decay_steps)
hparams.add_hparam("ec_temperature_decay_rate", FLAGS.ec_temperature_decay_rate)
hparams.add_hparam("ec_temperature_init", FLAGS.ec_temperature_init)
hparams.add_hparam("ec_temperature_lower_bound", FLAGS.ec_temperature_lower_bound)
hparams.add_hparam("ec_shared_coding", FLAGS.ec_shared_coding)
hparams.add_hparam("ec_code_dropout", FLAGS.ec_code_dropout)
hparams.add_hparam("ec_STE_softmax_transform", FLAGS.ec_STE_softmax_transform)
hparams.add_hparam("ec_hard_code_output", FLAGS.ec_hard_code_output)
hparams.add_hparam("ec_code_emb_dim", FLAGS.ec_code_emb_dim)
hparams.add_hparam("ec_aggregator", FLAGS.ec_aggregator)
hparams.add_hparam("ec_fnn_num_layers", FLAGS.ec_fnn_num_layers)
hparams.add_hparam("ec_fnn_hidden_size", FLAGS.ec_fnn_hidden_size)
hparams.add_hparam("ec_fnn_hidden_actv", FLAGS.ec_fnn_hidden_actv)
hparams.add_hparam("ec_cnn_num_layers", FLAGS.ec_cnn_num_layers)
hparams.add_hparam("ec_cnn_residual", FLAGS.ec_cnn_residual)
hparams.add_hparam("ec_cnn_pooling", FLAGS.ec_cnn_pooling)
hparams.add_hparam("ec_cnn_filters", FLAGS.ec_cnn_filters)
hparams.add_hparam("ec_cnn_kernel_size", FLAGS.ec_cnn_kernel_size)
hparams.add_hparam("ec_cnn_hidden_actv", FLAGS.ec_cnn_hidden_actv)
hparams.add_hparam("ec_rnn_num_layers", FLAGS.ec_rnn_num_layers)
hparams.add_hparam("ec_rnn_hidden_size", FLAGS.ec_rnn_hidden_size)
hparams.add_hparam("ec_rnn_additive_pooling", FLAGS.ec_rnn_additive_pooling)
hparams.add_hparam("ec_rnn_residual", FLAGS.ec_rnn_residual)
hparams.add_hparam("ec_rnn_dropout", FLAGS.ec_rnn_dropout)
hparams.add_hparam("ec_rnn_trainable_init_state", FLAGS.ec_rnn_trainable_init_state)
hparams.add_hparam("ec_rnn_bidirection", FLAGS.ec_rnn_bidirection)
hparams.add_hparam("ec_vq_reg_weight", FLAGS.ec_vq_reg_weight)
hparams.add_hparam("ec_emb_autoencoding", FLAGS.ec_emb_autoencoding)
hparams.add_hparam("ec_emb_transpose_layers", FLAGS.ec_emb_transpose_layers)
hparams.add_hparam("ec_emb_transpose_dim", FLAGS.ec_emb_transpose_dim)
hparams.add_hparam("ec_emb_transpose_actv", FLAGS.ec_emb_transpose_actv)

hparams.add_hparam("policy_optimizer", FLAGS.policy_optimizer)
hparams.add_hparam("policy_learning_rate", FLAGS.policy_learning_rate)
hparams.add_hparam("policy_var_learning_rate", FLAGS.policy_var_learning_rate)
hparams.add_hparam("policy_control_variate", FLAGS.policy_control_variate)
hparams.add_hparam("pg_num_samples", FLAGS.pg_num_samples)


def main(_):
  features_train, labels_train, _ = data.get_triple(
      FLAGS.dataset + "-train", FLAGS.batch_size,
      shuffle_buffer_size=100000, num_epochs=None, reinitializer=False,
      hparams=hparams)
  features_test, labels_test, init_op_test = data.get_triple(
      FLAGS.dataset + "-test", FLAGS.batch_size,
      shuffle_buffer_size=0, num_epochs=1, reinitializer=True)

  with tf.name_scope("Train"):
    with tf.variable_scope("model", reuse=False):
      m = Model(hparams, FLAGS.emb_load_filename, FLAGS.code_load_filename)
      loss_train, preds_train, train_op = m.forward(
          features_train, labels_train, is_training=True)
  with tf.name_scope("Test"):
    with tf.variable_scope("model", reuse=True):
      loss_test, preds_test, _ = m.forward(
          features_test, labels_test, is_training=False)

  # Verbose.
  print(hparams.values())
  print("Number of trainable params:    {}".format(
      util.get_parameter_count(
          excludings=["code_logits", "embb", "symbol2code"])))
  print(tf.trainable_variables())
  
  # Training session.
  #sv = tf.train.Supervisor(logdir=FLAGS.save_dir,
  #                         saver=None, save_summaries_secs=2)
  sv = tf.train.Supervisor(saver=None)  # DEBUG
  config_proto = tf.ConfigProto(allow_soft_placement=True)
  config_proto.gpu_options.allow_growth = True
  with sv.managed_session(config=config_proto) as sess:
    # Training loop.
    losses_train_ = []
    for it in range(FLAGS.max_iter):
      if FLAGS.test_summary:
        sess.run(init_op_test)
      loss_train_, _ = sess.run([loss_train, train_op])
      losses_train_.append(loss_train_)

      # Evaluation.
      if it % FLAGS.eval_every_num_iter == 0:
        sess.run(init_op_test)
        losses_test_ = []
        while True:
          try:
            losses_test_ .append(sess.run(loss_test))
          except tf.errors.OutOfRangeError:
            break
        rmse_train = np.sqrt(np.mean(np.concatenate(losses_train_)))
        rmse_test = np.sqrt(np.mean(np.concatenate(losses_test_)))
        print("Iter {:6}, RMSE_train {:.4}, RMSE_test {:.4}".format(
            it, rmse_train, rmse_test))
        losses_train_ = []

    # Save embeddings.
    if FLAGS.emb_save_filename is not None and FLAGS.emb_save_filename != "":
      embs = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
                               scope="model/embeddings/user_embedding")
      embs += tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,
                                scope="model/embeddings/item_embedding")
      if len(embs) != 2:
        raise ValueError("embs are {}".format(embs))
      embs = sess.run(embs)
      with open(FLAGS.emb_save_filename, "wb") as fp:
        pickle.dump(embs, fp, 2)

if __name__ == "__main__":
  tf.app.run()
