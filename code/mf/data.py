import os
import pandas as pd
import numpy as np
import cPickle as pickle
import tensorflow as tf

home = os.path.expanduser("~")


def get_triple(dataset,
               batch_size=1024,
               shuffle_buffer_size=0,
               num_epochs=1,
               reinitializer=False,
               hparams=None):
  """Returns triple tensor for given dataset.

  Args:
    dataset: A string description of the data, with parts separated by _.
    hparams: HParams object, if given will fill it that with data configuration.

  Returns:
    A user-item tensor of (batch_size, 2)
    A rating tensor of (batch_size, )
  """
  # Obtain the txt file containing triples.
  try:
    data_name, part = dataset.split("-")
  except:
    data_name, fold, part = dataset.split("-")
  assert data_name in ["movielens_100k", "movielens_1m", "movielens_10m",
                       "netflix"]
  assert part in ["train", "test"]
  if data_name in ["netflix"]:
    path = os.path.join(home, "dbase/recommendation", data_name, "split")
    part = "probe" if part == "test" else "train"
    path = os.path.join(path, "{}.pkl".format(part))
    with open(path) as fp:
      data = pickle.load(fp)
    ui_pairs = tf.convert_to_tensor(data[:, [0, 1]].astype("int32"))
    ratings = tf.convert_to_tensor(data[:, 2].astype("float32"))
    dataset = tf.data.Dataset.from_tensor_slices((ui_pairs, ratings))
    if hparams:
      hparams.add_hparam("num_users", max(data[:, 0])+1)
      hparams.add_hparam("num_items", max(data[:, 1])+1)
  else:
    path = os.path.join(home, "dbase/recommendation", data_name, "triple_cross")
    if data_name == "movielens_1m":
      path = os.path.join(path, "{}80_{}.txt".format(part, fold))
    else:
      path = os.path.join(path, "{}_{}.txt".format(part, fold))

    # Read the file and construct the tensor.
    # Implementation 1: faster, but requires all in memory.
    df = pd.read_csv(path, sep="\t", header=None, dtype={0:int, 1:int, 2:float})
    ui_pairs = tf.convert_to_tensor(df[[0, 1]].astype("int32"))
    ratings = tf.convert_to_tensor(df[2].astype("float32"))
    dataset = tf.data.Dataset.from_tensor_slices((ui_pairs, ratings))
    # Implementation 2: slower, seems map function too consuming.
    # ALSO: num_users, num_items not known.
    # dataset = tf.data.TextLineDataset(path, buffer_size=10000000)
    # dataset = dataset.map(_decode_line_of_triple, num_parallel_calls=3).cache()
    if hparams:
      hparams.add_hparam("num_users", max(df[0])+1)
      hparams.add_hparam("num_items", max(df[1])+1)

  dataset = dataset.repeat(num_epochs)
  if shuffle_buffer_size > 0:
    dataset = dataset.shuffle(buffer_size=shuffle_buffer_size)
  dataset = dataset.batch(batch_size)
  if reinitializer:
    iterator = tf.data.Iterator.from_structure(
        dataset.output_types, dataset.output_shapes)
    initializer = iterator.make_initializer(dataset)
  else:
    iterator = dataset.make_one_shot_iterator()
    initializer = None
  features, labels = iterator.get_next()
  return features, labels, initializer


def _decode_line_of_triple(line):
  record_defaults = [[-1], [-1], [-1.]]
  col1, col2, col3 = tf.decode_csv(
      line, record_defaults=record_defaults, field_delim="\t")
  features = tf.stack([col1, col2])
  labels = col3
  return features, labels


if __name__ == "__main__":
  import time
  dataset = "movielens_100k-1-train"
  start = time.time()
  features, labels, initializer = get_triple(
      dataset, shuffle_buffer_size=100000, num_epochs=1, reinitializer=True)
  with tf.Session() as sess:
    for _ in range(1):
      sess.run(initializer)
      sess.run([features, labels])
      try:
        while True:
          results = sess.run([features, labels])
      except tf.errors.OutOfRangeError:
        # print(results)
        print(time.time() - start)
