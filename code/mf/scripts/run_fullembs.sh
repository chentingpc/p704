#!/bin/bash
cd ..

gpu=$1
dataset=movielens_100k-1
dataset=movielens_1m-1
dataset=movielens_10m-1
dataset=netflix
save_root=~/pbase/x/p704/results/temp/
save_path=$save_root/$(date +"%m%d_%H%M%S")

max_iter=20000
optimizer=lazy_adam
learning_rate=1e-2
#optimizer=sgd
#learning_rate=10
#batch_size=1024
batch_size=4096
dims=300
reg_weight=5e-2
#reg_weight=0.0
use_recoding=False

# TODO: above is optimal for movielens-1m
# movielens_10m has best 0.6/0.98 train/test, odd.
# netflix needs to be loaded by feed_dict and variable due to its size.

#for learning_rate in 1. 1e-1 1e-2 1e-3; do
#for reg_weight in  1 1e-1 1e-2 1e-3; do
for learning_rate in 1e-1; do
for reg_weight in  0; do
	save_path=$save_root/$dataset\_lr$learning_rate\_reg$reg_weight
	# save_path=$save_root/$dataset
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python main.py --dataset=$dataset --save_dir=$save_path --max_iter=$max_iter --optimizer=$optimizer --batch_size=$batch_size --learning_rate=$learning_rate --reg_weight=$reg_weight --dims=$dims --use_recoding=$use_recoding #>$save_path/log 2>&1
done
done
