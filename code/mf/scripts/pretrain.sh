#!/bin/bash
cd ..

gpu=$1
dataset=movielens_100k-1
#dataset=movielens_1m-1
save_root=~/pbase/x/p704/results/temp/
save_path=$save_root/$(date +"%m%d_%H%M%S")

#optimizer=adam
optimizer=lazy_adam
learning_rate=1e-2
#optimizer=sgd
#learning_rate=1.
#optimizer=adagrad
#learning_rate=1
batch_size=1024
dims=100
reg_weight=1e-2
reg_weight=0.0
use_recoding=False
D=10
K=10
ec_logits_bn=1.
ec_code_emb_dim=10
ec_aggregator=mean
#ec_aggregator=mean_fnn
#ec_aggregator=rnn
ec_hard_code_output=True
ec_shared_coding=False
ec_STE_softmax_transform=True
ec_code_generator=STE_argmax
#ec_code_generator=gumbel_softmax
#ec_code_generator=relax
#ec_code_generator=preassign
ec_temperature_decay_method=none
#ec_temperature_decay_method=inv_time
ec_emb_baseline=True
ec_emb_baseline_reg=0.1
emb_save_filename="embs.pkl"
emb_load_filename=

CUDA_VISIBLE_DEVICES=$gpu python main.py --dataset=$dataset  --save_dir=$save_path --optimizer=$optimizer --batch_size=$batch_size --learning_rate=$learning_rate --reg_weight=$reg_weight --dims=$dims --use_recoding=$use_recoding --K=$K --D=$D --ec_code_emb_dim=$ec_code_emb_dim --ec_code_generator=$ec_code_generator --ec_aggregator=$ec_aggregator --ec_hard_code_output=$ec_hard_code_output --ec_logits_bn=$ec_logits_bn --ec_temperature_decay_method=$ec_temperature_decay_method --ec_STE_softmax_transform=$ec_STE_softmax_transform --ec_shared_coding=$ec_shared_coding --ec_emb_baseline=$ec_emb_baseline --ec_emb_baseline_reg=$ec_emb_baseline_reg --emb_save_filename=$emb_save_filename --emb_load_filename=$emb_load_filename
