import sys
import tensorflow as tf

sys.path.insert(0, "../lm")
import util
from encoder import Encoder

data_type = tf.float32


class Model(object):
  def __init__(self, hparams, emb_load_filename=None, code_load_filename=None):
    self._hparams = hparams
    self._use_recoding = hparams.use_recoding

    # Load pretrained embedding.
    if emb_load_filename is not None and emb_load_filename != "":
      with open(emb_load_filename) as fp:
        pretrained_embs = pickle.load(fp)
    else:
      pretrained_embs = None
    if pretrained_embs is not None:
      self._pretrained_user_embs, self._pretrained_item_embs = pretrained_embs
    else:
      self._pretrained_user_embs = self._pretrained_item_embs = None

    if hparams.use_recoding:
      with tf.variable_scope("user_var", reuse=False):
        user_encoder = Encoder(K=hparams.K,
                               D=hparams.D,
                               d=hparams.ec_code_emb_dim,
                               outd=hparams.dims,
                               hparams=hparams,
                               vocab_size=hparams.num_users,
                               code_type=hparams.code_type,
                               code_emb_initializer=None,
                               code_initializer=code_load_filename,
                               emb_baseline=hparams.ec_emb_baseline,
                               create_code_logits=not hparams.ec_emb_autoencoding,
                               pretrained_emb=self._pretrained_user_embs)

      with tf.variable_scope("item_var", reuse=False):
        item_encoder = Encoder(K=hparams.K,
                               D=hparams.D,
                               d=hparams.ec_code_emb_dim,
                               outd=hparams.dims,
                               hparams=hparams,
                               vocab_size=hparams.num_items,
                               code_type=hparams.code_type,
                               code_emb_initializer=None,
                               code_initializer=code_load_filename,
                               emb_baseline=hparams.ec_emb_baseline,
                               create_code_logits=not hparams.ec_emb_autoencoding,
                               pretrained_emb=self._pretrained_item_embs)

      if hparams.ec_code_generator == "preassign":
        self._ec_is_one_hot = False
      else:
        self._ec_is_one_hot = True

      if hparams.ec_code_generator == "pg":
        self._pg_first_n4f = hparams.pg_num_samples
      elif hparams.ec_code_generator == "relax":
        self._pg_first_n4f = 1
      self._user_encoder = user_encoder
      self._item_encoder = item_encoder

  def forward(self, features, labels, is_training=False):
    """Returns loss, preds, train_op.
    
    Args:
      features: (batch_size, 2)
      labels: (batch_size,)

    Returns:
      loss: (batch_size, )
      preds: (batch_size, )
      train_op: op.
    """
    hparams = self._hparams
    num_users = hparams.num_users
    num_items = hparams.num_items
    emb_dims = hparams.dims
    learning_rate = hparams.learning_rate
    reg_weight = hparams.reg_weight
    max_grad_norm = hparams.max_grad_norm

    batch_size = tf.shape(labels)[0]
    user_idx, item_idx = features[:, 0], features[:, 1]

    if not self._use_recoding:
      with tf.variable_scope("embeddings", reuse=not is_training):
        user_embedding = tf.get_variable(
            "user_embedding", [num_users, emb_dims], dtype=data_type)
        item_embedding = tf.get_variable(
            "item_embedding", [num_items, emb_dims], dtype=data_type)
        U = tf.nn.embedding_lookup(user_embedding, user_idx)
        #U = tf.layers.batch_normalization(U, training=is_training)
        V = tf.nn.embedding_lookup(item_embedding, item_idx)
        #V = tf.layers.batch_normalization(V, training=is_training)
        #I = tf.get_variable(  # DEBUG
        #    "bilinear_matrix", [emb_dims, emb_dims], dtype=data_type)
        #U = tf.matmul(U, I)
        """
        user_embedding = tf.get_variable(
            "user_embedding", [num_users, emb_dims / 5], dtype=data_type)
        item_embedding = tf.get_variable(
            "item_embedding", [num_items, emb_dims / 5], dtype=data_type)
        user_embedding2 = tf.get_variable(
            "user_embedding2", [emb_dims / 5, emb_dims], dtype=data_type)
        item_embedding2 = tf.get_variable(
            "item_embedding2", [emb_dims / 5, emb_dims], dtype=data_type)
        U = tf.nn.embedding_lookup(user_embedding, user_idx)
        U = tf.nn.softmax(U)
        U = tf.layers.batch_normalization(U, training=is_training)
        U = tf.matmul(U, user_embedding2)
        U = tf.layers.batch_normalization(U, training=is_training)
        V = tf.nn.embedding_lookup(item_embedding, item_idx)
        V = tf.nn.softmax(V)
        V = tf.layers.batch_normalization(V, training=is_training)
        V = tf.matmul(V, item_embedding2)
        V = tf.layers.batch_normalization(V, training=is_training)
        """
    else:
      with tf.variable_scope("user_var", reuse=not is_training):
        if hparams.ec_emb_baseline:
          code_logits = None
          if hparams.ec_emb_autoencoding:
            _embs = tf.nn.embedding_lookup(self._pretrained_user_embs, user_idx)
            code_logits = self._user_encoder.embed_transpose(
                _embs, is_training=is_training)
          user_codes, user_code_embs, embsb = self._user_encoder.symbol2code(
              user_idx, logits=code_logits,
              is_training=is_training, output_embb=True)
        else:
          embsb = None
          user_codes, user_code_embs = self._user_encoder.symbol2code(
              user_idx, is_training=is_training)
        U = self._user_encoder.embed(
            user_codes, code_embs=user_code_embs, embsb=embsb,
            is_one_hot=self._ec_is_one_hot, is_training=is_training)
      with tf.variable_scope("item_var", reuse=not is_training):
        if hparams.ec_emb_baseline:
          code_logits = None
          if hparams.ec_emb_autoencoding:
            _embs = tf.nn.embedding_lookup(self._pretrained_item_embs, item_idx)
            code_logits = self._item_encoder.embed_transpose(
                _embs, is_training=is_training)
          item_codes, item_code_embs, embsb = self._item_encoder.symbol2code(
              item_idx, logits=code_logits,
              is_training=is_training, output_embb=True)
        else:
          embsb = None
          item_codes, item_code_embs = self._item_encoder.symbol2code(
              item_idx, is_training=is_training)
        V = self._item_encoder.embed(
            item_codes, code_embs=item_code_embs, embsb=embsb,
            is_one_hot=self._ec_is_one_hot, is_training=is_training)
        """
        # DEBUG: For tuning
        #user_embedding = tf.get_variable(
        #    "user_embedding", [num_users, emb_dims], dtype=data_type)
        #U = tf.nn.embedding_lookup(user_embedding, user_idx)
        item_embedding = tf.get_variable(
            "item_embedding", [num_items, emb_dims], dtype=data_type)
        V = tf.nn.embedding_lookup(item_embedding, item_idx)
        """

    with tf.variable_scope("bias", reuse=not is_training):
      user_bias = tf.get_variable(
          "user_bias", [num_users, 1], dtype=data_type)
      item_bias = tf.get_variable(
          "item_bias", [num_items, 1], dtype=data_type)
    U_bias = tf.reshape(tf.nn.embedding_lookup(user_bias, user_idx), [-1])
    V_bias = tf.reshape(tf.nn.embedding_lookup(item_bias, item_idx), [-1])

    preds = tf.reduce_mean(tf.multiply(U, V), -1)
    #with tf.variable_scope("interaction", reuse=not is_training):  # DEBUG
    #  preds = tf.layers.dense(tf.layers.dense(tf.concat([U, V], -1), 100), 1)
    #  preds = tf.reshape(preds, [-1])
    preds += U_bias + V_bias
    loss = (labels - preds)**2  # (batch_size, ) or (pg_num_samples, batch_size)

    if is_training:
      # Add policy loss updater.
      if hparams.use_recoding and hparams.ec_code_generator in ["pg", "relax"]:
        policy_theta_user = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES,
            scope="model/user_var/code/code_logits")
        policy_theta_item = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES,
            scope="model/item_var/code/code_logits")
        if len(policy_theta_user) + len(policy_theta_item) != 2:
          raise ValueError("{},{}".format(policy_theta_user, policy_theta_item))
        tvars = tf.trainable_variables()
        non_pg_tvars = []
        for tvar in tvars:
          if not tvar.name.startswith("model/user_var/code") and (
              not tvar.name.startswith("model/item_var/code")):
            non_pg_tvars.append(tvar)
        if len(tvars) == len(non_pg_tvars):
          raise ValueError("")
        loss = tf.reshape(loss, [-1, batch_size])
        with tf.variable_scope("user_var"):
          train_op_policy_user = self._user_encoder._pgor.backward(
              loss, policy_theta_user)
        with tf.variable_scope("item_var"):
          train_op_policy_item = self._item_encoder._pgor.backward(
              loss, policy_theta_item)
        train_op_policy = tf.group(train_op_policy_user, train_op_policy_item)
        loss = tf.reshape(loss[:self._pg_first_n4f], [-1])
      else:
        non_pg_tvars = tf.trainable_variables()
        train_op_policy = None

      # Regular loss updater.
      #learning_rate = tf.train.inverse_time_decay(
      #    learning_rate, tf.train.get_or_create_global_step(),
      #    decay_steps=1000, decay_rate=1., staircase=True)
      #tf.summary.scalar("learning_rate", learning_rate)
      loss_scalar = tf.sqrt(tf.reduce_mean(loss))
      loss_scalar += reg_weight * (tf.reduce_mean(U**2) + tf.reduce_mean(V**2))
      loss_scalar += sum(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
      train_op = tf.contrib.layers.optimize_loss(
          loss=loss_scalar,
          global_step=tf.train.get_or_create_global_step(),
          learning_rate=learning_rate,
          optimizer=util.get_optimizer(hparams.optimizer),
          variables=non_pg_tvars,
          clip_gradients=max_grad_norm,
          summaries=["learning_rate", "loss", "global_gradient_norm"])
      if train_op_policy is not None:
        train_op = tf.group(train_op, train_op_policy)
    else:
      train_op = False
      if hparams.use_recoding and hparams.ec_code_generator in ["pg", "relax"]:
        loss = tf.reshape(loss, [-1, batch_size])
        loss = tf.reshape(loss[:self._pg_first_n4f], [-1])

    return loss, preds, train_op
