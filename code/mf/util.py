import numpy as np
import tensorflow as tf


data_type = tf.float32


def get_optimizer(name):
  name = name.lower()
  if name == "sgd":
    optimizer = tf.train.GradientDescentOptimizer
  elif name == "adam":
    optimizer = tf.train.AdamOptimizer
  elif name == "lazy_adam":
    optimizer = tf.contrib.opt.LazyAdamOptimizer
  elif name == "adagrad":
    optimizer = tf.train.AdagradOptimizer
  elif name == "rmsprop":
    optimizer = tf.train.RMSPropOptimizer
  else:
    raise ValueError("Unknown optimizer name {}.".format(name))

  return optimizer
