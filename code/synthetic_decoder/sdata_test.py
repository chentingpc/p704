import numpy as np
import tensorflow as tf
from sdata import SData


class SDataTest(tf.test.TestCase):

  def setUp(self):
    pass

  def testGenerationLowrank(self):
    vocab_size = 100
    n = 10
    method = "low-rank"
    sdata = SData(vocab_size=vocab_size, data_size_per_class=n, method=method,
                  feat_dim=10, latent_dim=2, seed=42)
    self._checkSTD(sdata.h, sdata.vocab_size, n, sdata._std_divider)
    with tf.Session() as sess:
      h, y = sdata.batch(batch_size=16)
      print(sess.run([h, y]))

  def testGenerationTree(self):
    vocab_size = None
    n = 10
    method = "tree"
    sdata = SData(vocab_size=vocab_size, data_size_per_class=n, method=method,
                  feat_dim=10, latent_dim=2, seed=42)
    self._checkSTD(sdata.h, sdata.vocab_size, n, sdata._std_divider)

  def _checkSTD(self, h, vocab_size, n, std_divider):
    # Test noises std should be smaller than the centroid std.
    std_across = []
    for i in range(vocab_size):
      std_across.append(h[n * i: n * (i + 1)].mean(axis=0))
      if i == 0:
        std_in = h[n * i: n].std(axis=0)
      else:
        std_in += h[n * i: n * (i + 1)].std(axis=0)

    std_in /= vocab_size
    std_across = np.std(std_across, axis=0) / std_divider
    self.assertAlmostEquals(std_in.mean(), std_across.mean(), delta=0.1)

if __name__ == "__main__":
  tf.test.main()
