"""Learn hash code using auto-encoder structure."""

import os
import sys
import time
import numpy as np
from sklearn import metrics
import cPickle as pickle
from collections import defaultdict, Counter
import tensorflow as tf

sys.path.insert(0, "../lm")
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # disable it when using tf.Print

import sdata
from encoder import Encoder
import util


FLAGS = tf.flags.FLAGS
flags = tf.flags
tf.flags.DEFINE_string("data_gen_method", "glove.6B.50d",
                        "One of the predefined method for generating data.")
tf.flags.DEFINE_integer("data_gen_seed", 42,
                        "Seed for random data generation.")
tf.flags.DEFINE_string("save_path", None,
                       "Path to save the checkpoints and events.")
tf.flags.DEFINE_integer("num_steps", 50000,
                        "Number of training steps.")
tf.flags.DEFINE_integer("output_iters", 1000,
                        "Wait for number of iterations for each output.")
tf.flags.DEFINE_integer("vocab_size", None,
                        "The number of classes/symbols.")
tf.flags.DEFINE_integer("data_size_per_class", None,
                        "The number of data points for each class.")
tf.flags.DEFINE_integer("feat_dim", 200,
                        "Feature h vector, i.e. data, dimensionality.")
tf.flags.DEFINE_integer("latent_dim", 10,
                        "latent_dim for data generation.")
tf.flags.DEFINE_integer("batch_size", 4096,
                        "The batch size to use.")
tf.flags.DEFINE_integer("num_clusters", 5,
                        "The number of clusters, if used.")
tf.flags.DEFINE_float("cluster_std", 1.0,
                        "The standard deviation of the clusters, if used.")
tf.flags.DEFINE_float("learning_rate", 1e-3,
                        "The learning rate.")
tf.flags.DEFINE_bool("duplicate_data_for_labels", True,
                     "Whether or not to duplicate the data_emb for vis labels.")
tf.flags.DEFINE_string("optimizer", "lazy_adam", "")
tf.flags.DEFINE_bool("test_summary", False, "")
tf.flags.DEFINE_bool("save_duplicated_code", False, "")

# Define encoder/decoder related.
flags.DEFINE_bool("use_x2z_encode", False, "Whether or not to use X2Z encoder.")
flags.DEFINE_bool("use_recoding", False, "Whether or not to use KD code.")
flags.DEFINE_integer("D", 10, "D-dimensional code.")
flags.DEFINE_integer("K", 100, "K-way code.")
flags.DEFINE_string("code_type", "redundant", "Type of the code.")
flags.DEFINE_string("code_save_filename", None, "")
flags.DEFINE_string("code_load_filename", None, "")
flags.DEFINE_string("emb_save_filename", None, "")
flags.DEFINE_string("emb_load_filename", None, "")
flags.DEFINE_string("ec_code_generator", "STE_argmax", "")
flags.DEFINE_bool("ec_emb_baseline", False, "")
flags.DEFINE_float("ec_emb_baseline_reg", 0., "")
flags.DEFINE_float("ec_emb_baseline_dropout", 0., "")
flags.DEFINE_float("ec_logits_bn", 0, "")
flags.DEFINE_float("ec_entropy_reg", 0., "")
flags.DEFINE_string("ec_temperature_decay_method", "none", "exp, inv_time")
flags.DEFINE_integer("ec_temperature_decay_steps", 500, "")
flags.DEFINE_float("ec_temperature_decay_rate", 1, "")
flags.DEFINE_float("ec_temperature_init", 1., "")
flags.DEFINE_float("ec_temperature_lower_bound", 1e-5, "")
flags.DEFINE_bool("ec_shared_coding", False, "")
flags.DEFINE_float("ec_code_dropout", None, "")
flags.DEFINE_bool("ec_STE_softmax_transform", True, "")
flags.DEFINE_bool("ec_hard_code_output", True, "")
flags.DEFINE_integer("ec_code_emb_dim", 100, "")
flags.DEFINE_string("ec_aggregator", "mean_fnn", "")
flags.DEFINE_integer("ec_fnn_num_layers", 1, "")
flags.DEFINE_integer("ec_fnn_hidden_size", 100, "")
flags.DEFINE_string("ec_fnn_hidden_actv", "relu", "")
flags.DEFINE_integer("ec_cnn_num_layers", 1, "")
flags.DEFINE_bool("ec_cnn_residual", True, "")
flags.DEFINE_string("ec_cnn_pooling", "max", "")
flags.DEFINE_integer("ec_cnn_filters", 100, "")
flags.DEFINE_integer("ec_cnn_kernel_size", 3, "")
flags.DEFINE_string("ec_cnn_hidden_actv", "relu", "")
flags.DEFINE_integer("ec_rnn_num_layers", 1, "")
flags.DEFINE_integer("ec_rnn_hidden_size", 100, "")
flags.DEFINE_bool("ec_rnn_additive_pooling", "tanh", "")
flags.DEFINE_bool("ec_rnn_residual", False, "")
flags.DEFINE_float("ec_rnn_dropout", 0., "")
flags.DEFINE_bool("ec_rnn_trainable_init_state", True, "")
flags.DEFINE_bool("ec_rnn_bidirection", False, "")
flags.DEFINE_float("ec_vq_reg_weight", 1., "")
flags.DEFINE_bool("ec_emb_autoencoding", False, "")
flags.DEFINE_integer("ec_emb_transpose_layers", 1, "")
flags.DEFINE_integer("ec_emb_transpose_dim", 100, "")
flags.DEFINE_string("ec_emb_transpose_actv", "tanh", "")

# Setup hyper-parameters.
hparams = tf.contrib.training.HParams()
hparams.add_hparam("data_gen_method", FLAGS.data_gen_method)
hparams.add_hparam("data_gen_seed", FLAGS.data_gen_seed)
hparams.add_hparam("num_steps", FLAGS.num_steps)
hparams.add_hparam("vocab_size", FLAGS.vocab_size)
hparams.add_hparam("data_size_per_class", FLAGS.data_size_per_class)
hparams.add_hparam("optimizer", FLAGS.optimizer)
hparams.add_hparam("learning_rate", FLAGS.learning_rate)
hparams.add_hparam("feat_dim", FLAGS.feat_dim)
hparams.add_hparam("latent_dim", FLAGS.latent_dim)
hparams.add_hparam("batch_size", FLAGS.batch_size)
hparams.add_hparam("num_clusters", FLAGS.num_clusters)
hparams.add_hparam("cluster_std", FLAGS.cluster_std)
hparams.add_hparam("test_summary", FLAGS.test_summary)
# Code related.
hparams.add_hparam("use_x2z_encode", FLAGS.use_x2z_encode)
hparams.add_hparam("use_recoding", FLAGS.use_recoding)
hparams.add_hparam("D", FLAGS.D)
hparams.add_hparam("K", FLAGS.K)
hparams.add_hparam("code_type", FLAGS.code_type)
hparams.add_hparam("code_save_filename", FLAGS.code_save_filename)
hparams.add_hparam("code_load_filename", FLAGS.code_load_filename)
hparams.add_hparam("emb_save_filename", FLAGS.emb_save_filename)
hparams.add_hparam("emb_load_filename", FLAGS.emb_load_filename)
hparams.add_hparam("ec_code_generator", FLAGS.ec_code_generator)
hparams.add_hparam("ec_emb_baseline", FLAGS.ec_emb_baseline)
hparams.add_hparam("ec_emb_baseline_reg", FLAGS.ec_emb_baseline_reg)
hparams.add_hparam("ec_emb_baseline_dropout", FLAGS.ec_emb_baseline_dropout)
hparams.add_hparam("ec_logits_bn", FLAGS.ec_logits_bn)
hparams.add_hparam("ec_entropy_reg", FLAGS.ec_entropy_reg)
hparams.add_hparam("ec_temperature_decay_method", FLAGS.ec_temperature_decay_method)
hparams.add_hparam("ec_temperature_decay_steps", FLAGS.ec_temperature_decay_steps)
hparams.add_hparam("ec_temperature_decay_rate", FLAGS.ec_temperature_decay_rate)
hparams.add_hparam("ec_temperature_init", FLAGS.ec_temperature_init)
hparams.add_hparam("ec_temperature_lower_bound", FLAGS.ec_temperature_lower_bound)
hparams.add_hparam("ec_shared_coding", FLAGS.ec_shared_coding)
hparams.add_hparam("ec_code_dropout", FLAGS.ec_code_dropout)
hparams.add_hparam("ec_STE_softmax_transform", FLAGS.ec_STE_softmax_transform)
hparams.add_hparam("ec_hard_code_output", FLAGS.ec_hard_code_output)
hparams.add_hparam("ec_code_emb_dim", FLAGS.ec_code_emb_dim)
hparams.add_hparam("ec_aggregator", FLAGS.ec_aggregator)
hparams.add_hparam("ec_fnn_num_layers", FLAGS.ec_fnn_num_layers)
hparams.add_hparam("ec_fnn_hidden_size", FLAGS.ec_fnn_hidden_size)
hparams.add_hparam("ec_fnn_hidden_actv", FLAGS.ec_fnn_hidden_actv)
hparams.add_hparam("ec_cnn_num_layers", FLAGS.ec_cnn_num_layers)
hparams.add_hparam("ec_cnn_residual", FLAGS.ec_cnn_residual)
hparams.add_hparam("ec_cnn_pooling", FLAGS.ec_cnn_pooling)
hparams.add_hparam("ec_cnn_filters", FLAGS.ec_cnn_filters)
hparams.add_hparam("ec_cnn_kernel_size", FLAGS.ec_cnn_kernel_size)
hparams.add_hparam("ec_cnn_hidden_actv", FLAGS.ec_cnn_hidden_actv)
hparams.add_hparam("ec_rnn_num_layers", FLAGS.ec_rnn_num_layers)
hparams.add_hparam("ec_rnn_hidden_size", FLAGS.ec_rnn_hidden_size)
hparams.add_hparam("ec_rnn_additive_pooling", FLAGS.ec_rnn_additive_pooling)
hparams.add_hparam("ec_rnn_residual", FLAGS.ec_rnn_residual)
hparams.add_hparam("ec_rnn_dropout", FLAGS.ec_rnn_dropout)
hparams.add_hparam("ec_rnn_trainable_init_state", FLAGS.ec_rnn_trainable_init_state)
hparams.add_hparam("ec_rnn_bidirection", FLAGS.ec_rnn_bidirection)
hparams.add_hparam("ec_vq_reg_weight", FLAGS.ec_vq_reg_weight)
hparams.add_hparam("ec_emb_autoencoding", FLAGS.ec_emb_autoencoding)
hparams.add_hparam("ec_emb_transpose_layers", FLAGS.ec_emb_transpose_layers)
hparams.add_hparam("ec_emb_transpose_dim", FLAGS.ec_emb_transpose_dim)
hparams.add_hparam("ec_emb_transpose_actv", FLAGS.ec_emb_transpose_actv)


def print_at_beginning(hparams):
  print("vocab_size={}, data_gen_method={}, num_clusters={}, feat_dim={}".format(
      hparams.vocab_size, hparams.data_gen_method,
      hparams.num_clusters, hparams.feat_dim))
  print("K={}, D={}, code_type={}".format(
      hparams.K, hparams.D, hparams.code_type))
  print("Number of original emb params: {}".format(
      hparams.vocab_size * hparams.feat_dim))
  print(hparams.values())
  print("Number of trainable params:    {}".format(
      util.get_parameter_count(excludings=None)))
  print(tf.trainable_variables())


def model(h, y, hparams):
  """Returns loss.

  Args:
    h: a feature `Tensor' of shape (batch_size, d)
    y: a label `Tensor` of shape (batch_size, ).

  Returns:
    loss and train_ops
  """
  vocab_size = hparams.vocab_size
  feat_dim = hparams.d # TODO

  W = tf.get_variable("W", [vocab_size, feat_dim])
  logits = tf.matmul(h, tf.transpose(W))  # (batch_size, vocab_size)
  preds = tf.nn.softmax(logits)
  #tf.one_hot(y, vocab_size) * util.safer_log(pred)
  loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits)
  train_op = tf.contrib.layers.optimize_loss(
      loss=loss,
      global_step=tf.train.get_or_create_global_step(),
      learning_rate=hparams.learning_rate,
      optimizer=util.get_optimizer(hparams.optimizer),
      summaries=["learning_rate", "loss", "global_gradient_norm"])
  return loss, train_op
  # DEBUG

  if hparams.ec_code_generator == "preassign":
    is_one_hot = False
  else:
    is_one_hot = True

  code_initializer = None
  if FLAGS.code_load_filename:
    if optimal_code is not None:
      raise ValueError("You can only use either code_load_filename or "
                       "optimal_code to initialize the code, but not both.")
    tf.logging.info("Using pre-trained code from file {}".format(
        FLAGS.code_load_filename))
    code_initializer = FLAGS.code_load_filename
  elif optimal_code is not None:
    tf.logging.info("Using optimal_code.")
    code_initializer = optimal_code

  # Critical Code class.
  encoder = Encoder(K=hparams.K,
                    D=hparams.D,
                    d=hparams.ec_code_emb_dim,
                    outd=hparams.feat_dim,
                    hparams=hparams,
                    vocab_size=hparams.vocab_size,
                    code_type=hparams.code_type,
                    code_initializer=code_initializer,
                    code_emb_initializer=None,
                    create_code_logits=not hparams.use_x2z_encode)

  # X->Z & Z->Xp.
  inputs = X if hparams.use_x2z_encode else idxs
  codes = embs_nn.X2Z(
      inputs, encoder, hparams, encode=hparams.use_x2z_encode, is_training=True)
  Xp = embs_nn.Z2X(
      codes, encoder, hparams, is_one_hot=is_one_hot, is_training=True)

  # Loss: negative ELBO.
  reconst_loss = tf.reduce_mean((Xp - X)**2)
  reg_loss = sum(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
  loss = reconst_loss + reg_loss

  #learning_rate = tf.train.exponential_decay(
  #    hparams.learning_rate, tf.train.get_or_create_global_step(),
  #    hparams.lr_decay_steps, hparams.lr_decay_rate, staircase=True)
  # summaries = None  # make it simple.
  train_op = tf.contrib.layers.optimize_loss(
      loss=loss,
      global_step=tf.train.get_or_create_global_step(),
      learning_rate=hparams.learning_rate,
      optimizer=util.get_optimizer(hparams.optimizer),
      summaries=["learning_rate", "loss", "global_gradient_norm"])

  # Additional ops
  if hparams.use_x2z_encode:
    # TODO: use batch for data_emb.
    query_codes = embs_nn.X2Z(
        data.data_emb, encoder, hparams, encode=True, is_training=False)
  else:
    query_codes = embs_nn.X2Z(
        tf.range(hparams.vocab_size),
        encoder, hparams, encode=False, is_training=False)
  aux_ops = {"query_codes": query_codes,
             "Xp": Xp}

  return loss, train_op, aux_ops


def main(_):

  # Get batched data tensor.
  data = sdata.SData(vocab_size=hparams.vocab_size,
                     data_size_per_class=hparams.data_size_per_class,
                     method=hparams.data_gen_method,
                     feat_dim=hparams.feat_dim,
                     latent_dim=hparams.latent_dim,
                     seed=hparams.data_gen_seed)
  hparams.vocab_size = data.vocab_size
  h, y = data.batch(batch_size=hparams.batch_size)


  # Build computation graph.
  loss, train_op, aux_ops = model(h, y, hparams)

  # Session and training.
  print_at_beginning(hparams)
  # sv = tf.train.Supervisor(logdir=FLAGS.save_path, save_summaries_secs=3)
  sv = tf.train.Supervisor(saver=None)  # DEBUG
  config_proto = tf.ConfigProto(allow_soft_placement=True,
                                inter_op_parallelism_threads=3)
  config_proto.gpu_options.allow_growth = True
  with sv.managed_session(config=config_proto) as sess:
    summary_writer = sv.summary_writer
    last_cs = None
    moving_loss = []
    for it in range(hparams.num_steps + 1):
      fetches = {"loss": loss,
                 "train_op": train_op}

      if it % FLAGS.output_iters != 0:
        vals = sess.run(fetches)
        moving_loss.append(vals["loss"])
      else:
        # Run and fetch results.
        fetches["query_codes"] = aux_ops["query_codes"]
        fetches["Xp"] = aux_ops["Xp"]
        fetches["X"] = embs
        vals = sess.run(fetches)
        moving_loss.append(vals["loss"])
        query_codes = vals["query_codes"]
        if len(query_codes.shape) > 2:
          # print(query_codes[0])  # print the softmax logits output.
          query_codes = np.argmax(query_codes, axis=-1)  # (vocab_size, D)
        summary = tf.Summary()

        # Plot recovery.
        if hparams.feat_dim == 2:
          try:
            _plot_original_X
          except:
            _plot_original_X = True
            sdata.plot_data(vals["X"], os.path.join(FLAGS.save_path, "X"))
          sdata.plot_data(vals["Xp"], os.path.join(FLAGS.save_path, "Xp-%d"%it))

        # Print some stats about the loss and the code.
        query_codes_set = set([tuple(each) for each in query_codes])
        unique_rate = float(len(query_codes_set)) / query_codes.shape[0]
        mean_loss = np.mean(np.sqrt(moving_loss))
        summary.value.add(tag="mean_loss", simple_value=mean_loss)
        print it, mean_loss,
        print "unique elements rate {}".format(unique_rate)
        summary.value.add(tag="unique_rate", simple_value=unique_rate)
        moving_loss = []

        # Print collision words.
        cs = ["-".join(each) for each in query_codes.astype('str').tolist()]
        cs_cnt = Counter(cs)
        if FLAGS.save_duplicated_code:
          duplicated = defaultdict(list)
          for i, code_ in enumerate(cs):
            if cs_cnt[code_] >= 1:  # use >=1 to save all codes, use >1 to save only duplicated ones.
              duplicated[code_].append(label[i])
          if FLAGS.save_path is None:
            path = './'
          else:
            path = FLAGS.save_path
          with open("{}/duplicated_{}".format(path, time.asctime()), "w") as fp:
            for code_ in sorted(duplicated):
              fp.write("{}\t\t{}\n".format(code_, duplicated[code_]))

        # Find out how many codes have been changed since last time here.
        if last_cs is not None:
          bit_diff = lambda x, y: np.sum(
              [0 if s == t else 1 for s, t in zip(x.split('-'), y.split('-'))])
          num_code_changed = np.sum(
              [0 if curt_c == past_c else 1 for curt_c, past_c in zip(
                  cs, last_cs)])
          num_bit_changed = np.sum(
              [bit_diff(curt_c, past_c) for curt_c, past_c in zip(cs, last_cs)])
          print("\t\tPercent of code changed {}".format(
              num_code_changed / float(len(cs))))
          print("\t\tPercent of bit changed per changed code {}".format(
              0 if num_code_changed == 0 else (
                  num_bit_changed / float(num_code_changed) / hparams.D)))
          summary.value.add(tag="percent_of_code_changed",
                            simple_value=num_code_changed / float(len(cs)))
          summary.value.add(
              tag="percent_of_bit_changed_per_changed_code",
              simple_value=0 if num_code_changed == 0 else (
                  num_bit_changed / float(num_code_changed) / hparams.D))

        last_cs = cs
        if summary_writer:
          summary_writer.add_summary(summary, it)
          summary_writer.flush()

    # Save code file.
    if FLAGS.code_save_filename:
      try:
        output_code_path = data.output_code_path + "K{}D{}.pkl".format(
          hparams.K, hparams.D)
      except:
        output_code_path = FLAGS.save_path + "code.K{}D{}.pkl".format(
          hparams.K, hparams.D)
      with open(output_code_path, 'w') as fp:
        pickle.dump(query_codes.astype(np.int16), fp, 2)

    if FLAGS.save_path and sv is not None:
      sv.saver.save(
          sess,
          os.path.join(FLAGS.save_path, "model"),
          global_step=sv.global_step)

      if vis_label_lists is not None:
        util.save_emb_visualize_meta(
            FLAGS.save_path, vis_var_names, vis_label_lists, vis_metadata_names)

if __name__ == "__main__":
  tf.app.run()
