"""Generating synthetic data."""
import numpy as np
import itertools
import tensorflow as tf


class SData(object):
  """Class for generating latent h features for different classes."""

  def __init__(self,
               vocab_size,
               data_size_per_class,
               method,
               feat_dim,
               latent_dim,
               seed=42):
    """
    Args:
      vocab_size: for tree, vocab_size is determined by latent_dim^feat_dim
      feat_dim: the dimensionality of h.
      latent_dim: for low-rank, it is the rank of h_centroids; for tree, it is
        the branching factor.
    """
    legit_methods = ["low-rank", "tree"]

    if method not in legit_methods:
      raise ValueError("method {} is not legit".format(method))
    if method == "tree":
      """
      self._K = int(np.ceil(np.power(vocab_size, 1./latent_dim)))
      print(vocab_size)
      vocab_size = int(np.power(self._K, latent_dim))
      print(vocab_size)
      """
      self._k = latent_dim
      vocab_size = int(np.power(latent_dim, feat_dim))

    self._method = method
    self._v = vocab_size
    self._n = data_size_per_class
    self._d = feat_dim
    self._dd = latent_dim
    self._seed = seed

    with tf.device("/cpu:0"):
      self._data_gen()

  def _data_gen(self):
    method = self._method
    if method == "low-rank":
      U = np.random.random((self._v, self._dd))
      V = np.random.random((self._dd, self._d))
      h_centroids = np.dot(U, V)
    elif method == "tree":
      h_centroids = itertools.product(range(self._k), repeat=self._d)
      h_centroids = np.array([each for each in h_centroids], dtype="float32")

    self._std_divider = 5.  # add noises that have much smaller std.
    std = h_centroids.std(0) / self._std_divider
    noises = np.random.normal(
      loc=np.zeros(self._d), scale=std, size=(self._n * self._v, self._d))
    h = np.repeat(h_centroids, self._n, 0) + noises
    self._h = h
    self._y = np.repeat(np.arange(self._v), self._n)

  def batch(self, batch_size, repeat=None):
    """Returns a batch of x and y."""
    with tf.device("/cpu:0"):
      idxs_data = tf.data.Dataset.range(self._v)
      idxs_data = idxs_data.shuffle(buffer_size=self._v)
      idxs_data = idxs_data.batch(batch_size).repeat(repeat)
      idxs_iterator = idxs_data.make_one_shot_iterator()
      idxs = idxs_iterator.get_next()
      h = tf.nn.embedding_lookup(self._h, idxs)
      y = tf.nn.embedding_lookup(self._y, idxs)
    return (h, y)

  @property
  def y(self):
    """Data point indicator, e.g. labels, cluster assignments, coordinates."""
    return self._y

  @property
  def h(self):
    return self._h

  @property
  def vocab_size(self):
    return self._v
