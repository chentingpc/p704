from layers import *
from metrics import *

import sys
sys.path.insert(0, "../lm")
from encoder import Encoder

flags = tf.app.flags
FLAGS = flags.FLAGS


class Model(object):
    def __init__(self, **kwargs):
        allowed_kwargs = {'name', 'logging'}
        for kwarg in kwargs.keys():
            assert kwarg in allowed_kwargs, 'Invalid keyword argument: ' + kwarg
        name = kwargs.get('name')
        if not name:
            name = self.__class__.__name__.lower()
        self.name = name

        logging = kwargs.get('logging', False)
        self.logging = logging

        self.vars = {}
        self.placeholders = {}

        self.layers = []
        self.activations = []

        self.inputs = None
        self.outputs = None

        self.loss = 0
        self.accuracy = 0
        self.optimizer = None
        self.opt_op = None

    def _build(self):
        raise NotImplementedError

    def build(self):
        """ Wrapper for _build() """
        with tf.variable_scope(self.name):
            self._build(is_training=True)

        # Build sequential layer model
        self.activations.append(self.inputs)
        for layer in self.layers:
            hidden = layer(self.activations[-1])
            self.activations.append(hidden)
        self.outputs = self.activations[-1]

        # Store model variables for easy access
        variables = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.name)
        self.vars = {var.name: var for var in variables}

        # Build metrics
        self._loss()
        #self._accuracy()

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            self.loss += sum(
                    tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
            self.opt_op = self.optimizer.minimize(self.loss)

        # Build graph "again" for inference (excluding baseline embedding etc.).
        self.layers = []
        self.activations = []
        self.accuracy = 0
        self.outputs = None
        with tf.variable_scope(self.name, reuse=True):
            self._build(is_training=False)
        self.activations.append(self.inputs)
        for layer in self.layers:
            hidden = layer(self.activations[-1])
            self.activations.append(hidden)
        self.outputs = self.activations[-1]
        self._accuracy()


    def predict(self):
        pass

    def _loss(self):
        raise NotImplementedError

    def _accuracy(self):
        raise NotImplementedError

    def save(self, sess=None):
        if not sess:
            raise AttributeError("TensorFlow session not provided.")
        saver = tf.train.Saver(self.vars)
        save_path = saver.save(sess, "tmp/%s.ckpt" % self.name)
        print("Model saved in file: %s" % save_path)

    def load(self, sess=None):
        if not sess:
            raise AttributeError("TensorFlow session not provided.")
        saver = tf.train.Saver(self.vars)
        save_path = "tmp/%s.ckpt" % self.name
        saver.restore(sess, save_path)
        print("Model restored from file: %s" % save_path)


class MLP(Model):
    def __init__(self, placeholders, input_dim, **kwargs):
        super(MLP, self).__init__(**kwargs)

        self.inputs = placeholders['features']
        self.input_dim = input_dim
        # self.input_dim = self.inputs.get_shape().as_list()[1]  # To be supported in future Tensorflow versions
        self.output_dim = placeholders['labels'].get_shape().as_list()[1]
        self.placeholders = placeholders

        self.optimizer = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate)

        self.build()

    def _loss(self):
        # Weight decay loss
        for var in self.layers[0].vars.values():
            self.loss += FLAGS.weight_decay * tf.nn.l2_loss(var)

        # Cross entropy error
        self.loss += masked_softmax_cross_entropy(self.outputs, self.placeholders['labels'],
                                                  self.placeholders['labels_mask'])

    def _accuracy(self):
        self.accuracy = masked_accuracy(self.outputs, self.placeholders['labels'],
                                        self.placeholders['labels_mask'])

    def _build(self):
        self.layers.append(Dense(input_dim=self.input_dim,
                                 output_dim=FLAGS.hidden1,
                                 placeholders=self.placeholders,
                                 act=tf.nn.relu,
                                 dropout=True,
                                 sparse_inputs=True,
                                 logging=self.logging))

        self.layers.append(Dense(input_dim=FLAGS.hidden1,
                                 output_dim=self.output_dim,
                                 placeholders=self.placeholders,
                                 act=lambda x: x,
                                 dropout=True,
                                 logging=self.logging))

    def predict(self):
        return tf.nn.softmax(self.outputs)


class GCN(Model):
    def __init__(self, placeholders, input_dim, hparams, **kwargs):
        super(GCN, self).__init__(**kwargs)

        self.inputs = placeholders['features']
        self.input_dim = input_dim
        # self.input_dim = self.inputs.get_shape().as_list()[1]  # To be supported in future Tensorflow versions
        self.output_dim = placeholders['labels'].get_shape().as_list()[1]
        self.placeholders = placeholders

        self.optimizer = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate)

        self.hparams = hparams

        self.build()

    def _loss(self):
        # Weight decay loss
        for var in self.layers[0].vars.values():
            self.loss += FLAGS.weight_decay * tf.nn.l2_loss(var)

        # Cross entropy error
        self.loss += masked_softmax_cross_entropy(self.outputs, self.placeholders['labels'],
                                                  self.placeholders['labels_mask'])

    def _accuracy(self):
        self.accuracy = masked_accuracy(self.outputs, self.placeholders['labels'],
                                        self.placeholders['labels_mask'])

    def _build(self, is_training=True):

        # Add KD encoding.
        hparams = self.hparams
        weights = None
        vocab_size = self.input_dim
        emb_dim = FLAGS.hidden1
        if hparams.use_recoding:
            assert len(self.placeholders['support']) == 1, "NotImplementedError"
            pretrained_emb = None # DEBUG
            # with tf.variable_scope("disco_embeddings", reuse=False):
            if is_training:
                self._encoder = Encoder(K=hparams.K,
                                        D=hparams.D,
                                        d=hparams.ec_code_emb_dim,
                                        outd=emb_dim,
                                        hparams=hparams,
                                        vocab_size=vocab_size,
                                        code_type=hparams.code_type,
                                        code_emb_initializer=None,
                                        emb_baseline=hparams.ec_emb_baseline,
                                        create_code_logits=not hparams.ec_emb_autoencoding,
                                        pretrained_emb=pretrained_emb)

                if hparams.ec_code_generator == "preassign":
                    self._ec_is_one_hot = False
                else:
                    self._ec_is_one_hot = True

            if hparams.ec_emb_baseline:
                code_logits = None
                if hparams.ec_emb_autoencoding:
                    _embs = tf.nn.embedding_lookup(pretrained_emb, tf.range(vocab_size))
                    code_logits = self._encoder.embed_transpose(_embs, is_training=is_training)
                codes, code_embs, embsb = self._encoder.symbol2code(
                        tf.range(vocab_size),
                        is_training=is_training,
                        logits=code_logits,
                        output_embb=True)
            else:
                embsb = None
                codes, code_embs = self._encoder.symbol2code(
                        tf.range(vocab_size), is_training=is_training)
            embs = self._encoder.embed(
                    codes, code_embs=code_embs, embsb=embsb,
                    is_one_hot=self._ec_is_one_hot, is_training=is_training)
            weights = [embs]
        elif hparams.emb_lowrank_dim != 0:
            # Low rank approximate the embedding matrix.
            _dim = hparams.emb_lowrank_dim
            if _dim < 1.:  # Represents ratio of low-rank parameters to full one.
                _dim = int(_dim * vocab_size / (vocab_size + emb_dim) * emb_dim)
            embedding_p = tf.get_variable(
                "embedding_p", [vocab_size, _dim], dtype=tf.float32)
            embedding_q = tf.get_variable(
                "embedding_q", [_dim, emb_dim], dtype=tf.float32)
            weights = [tf.matmul(embedding_p, embedding_q)]

        self.layers.append(GraphConvolution(input_dim=self.input_dim,
                                            output_dim=FLAGS.hidden1,
                                            placeholders=self.placeholders,
                                            act=tf.nn.relu,
                                            dropout=True,
                                            sparse_inputs=True,
                                            weights=weights,
                                            logging=self.logging,
                                            name="gcnl1"))

        self.layers.append(GraphConvolution(input_dim=FLAGS.hidden1,
                                            output_dim=self.output_dim,
                                            placeholders=self.placeholders,
                                            act=lambda x: x,
                                            dropout=True,
                                            logging=self.logging,
                                            name="gcnl2"))

    def predict(self):
        return tf.nn.softmax(self.outputs)
