from __future__ import division
from __future__ import print_function

import time
import tensorflow as tf

import sys
sys.path.insert(0, "../lm")
from util import get_parameter_count

from utils import *
from models import GCN, MLP

# Set random seed
seed = 123
np.random.seed(seed)
tf.set_random_seed(seed)

# Settings
flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('dataset', 'cora', 'Dataset string.')  # 'cora', 'citeseer', 'pubmed'
flags.DEFINE_string('model', 'gcn', 'Model string.')  # 'gcn', 'gcn_cheby', 'dense'
flags.DEFINE_float('learning_rate', 0.01, 'Initial learning rate.')
flags.DEFINE_integer('epochs', 300, 'Number of epochs to train.')
flags.DEFINE_integer('hidden1', 16, 'Number of units in hidden layer 1.')
flags.DEFINE_float('dropout', 0.5, 'Dropout rate (1 - keep probability).')
flags.DEFINE_float('weight_decay', 5e-4, 'Weight for L2 loss on embedding matrix.')
flags.DEFINE_integer('early_stopping', 10, 'Tolerance for early stopping (# of epochs).')
flags.DEFINE_integer('max_degree', 3, 'Maximum Chebyshev polynomial degree.')
flags.DEFINE_float('stop_at_valid', 0, 'Stopping at the validation accuracy.')
flags.DEFINE_float("emb_lowrank_dim", 0, "")

# Code related
flags.DEFINE_bool("use_recoding", False, "Whether or not to use KD code.")
flags.DEFINE_integer("D", 10, "D-dimensional code.")
flags.DEFINE_integer("K", 100, "K-way code.")
flags.DEFINE_string("code_type", "redundant", "Type of the code.")
flags.DEFINE_string("code_save_filename", None, "")
flags.DEFINE_string("code_load_filename", None, "")
flags.DEFINE_string("emb_save_filename", None, "")
flags.DEFINE_string("emb_load_filename", None, "")
flags.DEFINE_string("ec_code_generator", "STE_argmax", "")
flags.DEFINE_bool("ec_emb_baseline", False, "")
flags.DEFINE_float("ec_emb_baseline_reg", 0., "")
flags.DEFINE_float("ec_emb_baseline_dropout", 0., "")
flags.DEFINE_float("ec_logits_bn", 0, "")
flags.DEFINE_float("ec_entropy_reg", 0., "")
flags.DEFINE_string("ec_temperature_decay_method", "none", "exp, inv_time")
flags.DEFINE_integer("ec_temperature_decay_steps", 500, "")
flags.DEFINE_float("ec_temperature_decay_rate", 1, "")
flags.DEFINE_float("ec_temperature_init", 1., "")
flags.DEFINE_float("ec_temperature_lower_bound", 1e-5, "")
flags.DEFINE_bool("ec_shared_coding", False, "")
flags.DEFINE_float("ec_code_dropout", None, "")
flags.DEFINE_bool("ec_STE_softmax_transform", True, "")
flags.DEFINE_bool("ec_hard_code_output", True, "")
flags.DEFINE_integer("ec_code_emb_dim", 100, "")
flags.DEFINE_string("ec_aggregator", "mean_fnn", "")
flags.DEFINE_integer("ec_fnn_num_layers", 1, "")
flags.DEFINE_integer("ec_fnn_hidden_size", 100, "")
flags.DEFINE_string("ec_fnn_hidden_actv", "tanh", "")
flags.DEFINE_integer("ec_cnn_num_layers", 1, "")
flags.DEFINE_bool("ec_cnn_residual", True, "")
flags.DEFINE_string("ec_cnn_pooling", "max", "")
flags.DEFINE_integer("ec_cnn_filters", 100, "")
flags.DEFINE_integer("ec_cnn_kernel_size", 3, "")
flags.DEFINE_string("ec_cnn_hidden_actv", "tanh", "")
flags.DEFINE_integer("ec_rnn_num_layers", 1, "")
flags.DEFINE_integer("ec_rnn_hidden_size", 100, "")
flags.DEFINE_bool("ec_rnn_additive_pooling", "tanh", "")
flags.DEFINE_bool("ec_rnn_residual", False, "")
flags.DEFINE_float("ec_rnn_dropout", 0., "")
flags.DEFINE_bool("ec_rnn_trainable_init_state", True, "")
flags.DEFINE_bool("ec_rnn_bidirection", False, "")
flags.DEFINE_float("ec_vq_reg_weight", 1., "")
flags.DEFINE_bool("ec_emb_autoencoding", False, "")
flags.DEFINE_integer("ec_emb_transpose_layers", 1, "")
flags.DEFINE_integer("ec_emb_transpose_dim", 100, "")
flags.DEFINE_string("ec_emb_transpose_actv", "tanh", "")

# Code related hparams.
hparams = tf.contrib.training.HParams()
hparams.add_hparam("emb_lowrank_dim", FLAGS.emb_lowrank_dim)
hparams.add_hparam("use_recoding", FLAGS.use_recoding)
hparams.add_hparam("D", FLAGS.D)
hparams.add_hparam("K", FLAGS.K)
hparams.add_hparam("code_type", FLAGS.code_type)
hparams.add_hparam("code_save_filename", FLAGS.code_save_filename)
hparams.add_hparam("code_load_filename", FLAGS.code_load_filename)
hparams.add_hparam("emb_save_filename", FLAGS.emb_save_filename)
hparams.add_hparam("emb_load_filename", FLAGS.emb_load_filename)
hparams.add_hparam("ec_code_generator", FLAGS.ec_code_generator)
hparams.add_hparam("ec_emb_baseline", FLAGS.ec_emb_baseline)
hparams.add_hparam("ec_emb_baseline_reg", FLAGS.ec_emb_baseline_reg)
hparams.add_hparam("ec_emb_baseline_dropout", FLAGS.ec_emb_baseline_dropout)
hparams.add_hparam("ec_logits_bn", FLAGS.ec_logits_bn)
hparams.add_hparam("ec_entropy_reg", FLAGS.ec_entropy_reg)
hparams.add_hparam("ec_temperature_decay_method", FLAGS.ec_temperature_decay_method)
hparams.add_hparam("ec_temperature_decay_steps", FLAGS.ec_temperature_decay_steps)
hparams.add_hparam("ec_temperature_decay_rate", FLAGS.ec_temperature_decay_rate)
hparams.add_hparam("ec_temperature_init", FLAGS.ec_temperature_init)
hparams.add_hparam("ec_temperature_lower_bound", FLAGS.ec_temperature_lower_bound)
hparams.add_hparam("ec_shared_coding", FLAGS.ec_shared_coding)
hparams.add_hparam("ec_code_dropout", FLAGS.ec_code_dropout)
hparams.add_hparam("ec_STE_softmax_transform", FLAGS.ec_STE_softmax_transform)
hparams.add_hparam("ec_hard_code_output", FLAGS.ec_hard_code_output)
hparams.add_hparam("ec_code_emb_dim", FLAGS.ec_code_emb_dim)
hparams.add_hparam("ec_aggregator", FLAGS.ec_aggregator)
hparams.add_hparam("ec_fnn_num_layers", FLAGS.ec_fnn_num_layers)
hparams.add_hparam("ec_fnn_hidden_size", FLAGS.ec_fnn_hidden_size)
hparams.add_hparam("ec_fnn_hidden_actv", FLAGS.ec_fnn_hidden_actv)
hparams.add_hparam("ec_cnn_num_layers", FLAGS.ec_cnn_num_layers)
hparams.add_hparam("ec_cnn_residual", FLAGS.ec_cnn_residual)
hparams.add_hparam("ec_cnn_pooling", FLAGS.ec_cnn_pooling)
hparams.add_hparam("ec_cnn_filters", FLAGS.ec_cnn_filters)
hparams.add_hparam("ec_cnn_kernel_size", FLAGS.ec_cnn_kernel_size)
hparams.add_hparam("ec_cnn_hidden_actv", FLAGS.ec_cnn_hidden_actv)
hparams.add_hparam("ec_rnn_num_layers", FLAGS.ec_rnn_num_layers)
hparams.add_hparam("ec_rnn_hidden_size", FLAGS.ec_rnn_hidden_size)
hparams.add_hparam("ec_rnn_additive_pooling", FLAGS.ec_rnn_additive_pooling)
hparams.add_hparam("ec_rnn_residual", FLAGS.ec_rnn_residual)
hparams.add_hparam("ec_rnn_dropout", FLAGS.ec_rnn_dropout)
hparams.add_hparam("ec_rnn_trainable_init_state", FLAGS.ec_rnn_trainable_init_state)
hparams.add_hparam("ec_rnn_bidirection", FLAGS.ec_rnn_bidirection)
hparams.add_hparam("ec_vq_reg_weight", FLAGS.ec_vq_reg_weight)
hparams.add_hparam("ec_emb_autoencoding", FLAGS.ec_emb_autoencoding)
hparams.add_hparam("ec_emb_transpose_layers", FLAGS.ec_emb_transpose_layers)
hparams.add_hparam("ec_emb_transpose_dim", FLAGS.ec_emb_transpose_dim)
hparams.add_hparam("ec_emb_transpose_actv", FLAGS.ec_emb_transpose_actv)

# Load data
adj, features, y_train, y_val, y_test, train_mask, val_mask, test_mask = load_data(FLAGS.dataset)

# Some preprocessing
features = preprocess_features(features)
if FLAGS.model == 'gcn':
    support = [preprocess_adj(adj)]
    num_supports = 1
    model_func = GCN
elif FLAGS.model == 'gcn_cheby':
    support = chebyshev_polynomials(adj, FLAGS.max_degree)
    num_supports = 1 + FLAGS.max_degree
    model_func = GCN
elif FLAGS.model == 'dense':
    support = [preprocess_adj(adj)]  # Not used
    num_supports = 1
    model_func = MLP
else:
    raise ValueError('Invalid argument for model: ' + str(FLAGS.model))

# Define placeholders
placeholders = {
    'support': [tf.sparse_placeholder(tf.float32) for _ in range(num_supports)],
    'features': tf.sparse_placeholder(tf.float32, shape=tf.constant(features[2], dtype=tf.int64)),
    'labels': tf.placeholder(tf.float32, shape=(None, y_train.shape[1])),
    'labels_mask': tf.placeholder(tf.int32),
    'dropout': tf.placeholder_with_default(0., shape=()),
    'num_features_nonzero': tf.placeholder(tf.int32)  # helper variable for sparse dropout
}

# Create model
model = model_func(placeholders, input_dim=features[2][1], hparams=hparams, logging=True)

# Print out.
print(hparams.values())
print("Number of trainable params:    {}".format(
    get_parameter_count(excludings=["code_logits", "embb", "symbol2code"])))
print(tf.trainable_variables())

# Initialize session
config_proto = tf.ConfigProto(allow_soft_placement=True)
config_proto.gpu_options.allow_growth = True
sess = tf.Session(config=config_proto)


# Define model evaluation function
def evaluate(features, support, labels, mask, placeholders):
    t_test = time.time()
    feed_dict_val = construct_feed_dict(features, support, labels, mask, placeholders)
    # outs_val = sess.run([model.loss, model.accuracy], feed_dict=feed_dict_val)
    outs_val = sess.run([model.loss], feed_dict=feed_dict_val)
    outs_val += sess.run([model.accuracy], feed_dict=feed_dict_val)
    return outs_val[0], outs_val[1], (time.time() - t_test)


# Init variables
sess.run(tf.global_variables_initializer())

cost_val = []

# Train model
for epoch in range(FLAGS.epochs):

    t = time.time()
    # Construct feed dictionary
    feed_dict = construct_feed_dict(features, support, y_train, train_mask, placeholders)
    feed_dict.update({placeholders['dropout']: FLAGS.dropout})

    # Training step
    outs = sess.run([model.opt_op, model.loss, model.accuracy], feed_dict=feed_dict)

    # Validation
    cost, acc, duration = evaluate(features, support, y_val, val_mask, placeholders)
    cost_val.append(cost)

    # Print results
    print("Epoch:", '%04d' % (epoch + 1), "train_loss=", "{:.5f}".format(outs[1]),
          "train_acc=", "{:.5f}".format(outs[2]), "val_loss=", "{:.5f}".format(cost),
          "val_acc=", "{:.5f}".format(acc), "time=", "{:.5f}".format(time.time() - t))

    if FLAGS.stop_at_valid != 0:
        if acc >= FLAGS.stop_at_valid:
            break
    elif epoch > FLAGS.early_stopping and cost_val[-1] > np.mean(cost_val[-(FLAGS.early_stopping+1):-1]):
        print("Early stopping...")
        break

print("Optimization Finished!")

# Testing
test_cost, test_acc, test_duration = evaluate(features, support, y_test, test_mask, placeholders)
print("Test set results:", "cost=", "{:.5f}".format(test_cost),
      "accuracy=", "{:.5f}".format(test_acc), "time=", "{:.5f}".format(test_duration))
