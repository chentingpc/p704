#!/bin/bash
###########################################################################
# This script is used to generate full embedding and low-rank baselines.
# Options:
#   dataset: which dataset to run.
#   emb_lowrank_dim: the ratio of parameters to be kept when it is in (0, 1).
# Any other options should be leaved as default.
# Others:
#   emb_size: cora:1433*16 citeseer:3703*16 pubmed:500*16.
###########################################################################
cd ..

save_root=~/pbase/x/p704/results/temp/
learning_rate=1e-2
reg_weight=0
use_recoding=False
emb_lowrank_dim=0.

for dataset in cora citeseer pubmed; do
	save_path=$save_root/$dataset
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	stdbuf -oL -eL python train.py --dataset=$dataset --learning_rate=$learning_rate --reg_weight=$reg_weight --use_recoding=$use_recoding --emb_lowrank_dim=$emb_lowrank_dim >$save_path/log 2>&1
done
