#!/bin/bash
###########################################################################
# This script is used to generate kd encoding with different methods.
# Options:
#   dataset: which dataset to run.
#   run_mode: random_code, no_guidance.
# Any other options should be leaved as default.
# Others:
#   emb_size: cora:1433*16 citeseer:3703*16 pubmed:500*16.
#   relevant params: KD, ec_emb_baseline related and entropy.
###########################################################################
cd ..

gpu=$1
save_root=~/pbase/x/p704/results/temp/
learning_rate=1e-2
reg_weight=0
use_recoding=True
ec_logits_bn=1.
ec_code_dropout=0.
ec_fnn_hidden_size=16
ec_hard_code_output=True
ec_shared_coding=False
ec_STE_softmax_transform=True

for run_mode in random_code no_guidance; do
for dataset in cora citeseer pubmed; do
	if [ $dataset == cora ]; then
		D=8
		K=64
		ec_code_emb_dim=16
		ec_aggregator=mean
	    ec_emb_baseline=False
		ec_emb_baseline_reg=0.1
		stop_at_valid=0.81
	elif [ $dataset == citeseer ]; then
		D=8
		K=64
		ec_code_emb_dim=16
		ec_aggregator=mean
	    ec_emb_baseline=False
		ec_emb_baseline_reg=0.1
		stop_at_valid=0.73
	elif [ $dataset == pubmed ]; then
		D=4
		K=32
		ec_code_emb_dim=16
		ec_aggregator=mean_fnn
	    ec_emb_baseline=False
		ec_emb_baseline_reg=0.1
		stop_at_valid=0.81
	fi

	if [ $run_mode == random_code ]; then
		ec_code_generator=preassign
		ec_emb_baseline=False
		ec_entropy_reg=0.
		stop_at_valid=0.
	elif [ $run_mode == no_guidance ]; then
		ec_code_generator=STE_argmax
		#ec_code_generator=gumbel_softmax
		ec_entropy_reg=0.1
	fi

	save_path=$save_root/$run_mode\_$dataset
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python train.py --dataset=$dataset --learning_rate=$learning_rate --reg_weight=$reg_weight --use_recoding=$use_recoding --K=$K --D=$D --ec_code_emb_dim=$ec_code_emb_dim --ec_code_generator=$ec_code_generator --ec_aggregator=$ec_aggregator --ec_hard_code_output=$ec_hard_code_output --ec_logits_bn=$ec_logits_bn --ec_STE_softmax_transform=$ec_STE_softmax_transform --ec_shared_coding=$ec_shared_coding --ec_code_dropout=$ec_code_dropout --ec_emb_baseline=$ec_emb_baseline --ec_emb_baseline_reg=$ec_emb_baseline_reg --ec_entropy_reg=$ec_entropy_reg --ec_fnn_hidden_size=$ec_fnn_hidden_size --stop_at_valid=$stop_at_valid >$save_path/log 2>&1
done
done
