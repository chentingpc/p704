import tensorflow as tf
 

def static_rnn(cell, inputs, initial_state, weight_sharing=False):
  """Static rnn allows different weights per time stamp."""
  num_steps = len(inputs)
  state = initial_state
  outputs = []
  with tf.variable_scope("RNN"):
    for time_step, _input in enumerate(inputs):
      if weight_sharing and time_step > 0:
        tf.get_variable_scope().reuse_variables()
      (cell_output, state) = cell(_input, state)
      outputs.append(cell_output)
  return outputs, state


def _linear(args,
            output_size,
            bias,
            bias_initializer=None,
            kernel_initializer=None):
  """Linear map: sum_i(args[i] * W[i]), where W[i] is a variable.
  Args:
    args: a 2D Tensor or a list of 2D, batch x n, Tensors.
    output_size: int, second dimension of W[i].
    bias: boolean, whether to add a bias term or not.
    bias_initializer: starting value to initialize the bias
      (default is all zeros).
    kernel_initializer: starting value to initialize the weight.
  Returns:
    A 2D Tensor with shape [batch x output_size] equal to
    sum_i(args[i] * W[i]), where W[i]s are newly created matrices.
  Raises:
    ValueError: if some of the arguments has unspecified or wrong shape.
  """
  from  tensorflow.python.util import nest
  _BIAS_VARIABLE_NAME = "bias"
  _WEIGHTS_VARIABLE_NAME = "kernel"

  if args is None or (nest.is_sequence(args) and not args):
    raise ValueError("`args` must be specified")
  if not nest.is_sequence(args):
    args = [args]

  # Calculate the total size of arguments on dimension 1.
  total_arg_size = 0
  shapes = [a.get_shape() for a in args]
  for shape in shapes:
    if shape.ndims != 2:
      raise ValueError("linear is expecting 2D arguments: %s" % shapes)
    if shape[1].value is None:
      raise ValueError("linear expects shape[1] to be provided for shape %s, "
                       "but saw %s" % (shape, shape[1]))
    else:
      total_arg_size += shape[1].value

  dtype = [a.dtype for a in args][0]

  # Now the computation.
  scope = tf.get_variable_scope()
  with tf.variable_scope(scope) as outer_scope:
    weights = tf.get_variable(
        _WEIGHTS_VARIABLE_NAME, [total_arg_size, output_size],
        dtype=dtype,
        initializer=kernel_initializer)
    if len(args) == 1:
      res = tf.matmul(args[0], weights)
    else:
      res = tf.matmul(tf.concat(args, 1), weights)
    if not bias:
      return res
    with tf.variable_scope(outer_scope) as inner_scope:
      inner_scope.set_partitioner(None)
      if bias_initializer is None:
        bias_initializer = tf.constant_initializer(0.0, dtype=dtype)
      biases = tf.get_variable(
          _BIAS_VARIABLE_NAME, [output_size],
          dtype=dtype,
          initializer=bias_initializer)
    return tf.nn.bias_add(res, biases)


class DampingLSTMCell(tf.contrib.rnn.BasicLSTMCell):
  """Damping factor added BasicLSTMCell.

  The only thing change is to damp the incoming hidden state along steps.
  The output gate is also disabled.

  To use the class, require reset_step ever time it is run, e.g. in static_rnn.
  """

  def __init__(self, num_units, damping_factor, forget_bias=1.0,
               state_is_tuple=True, activation=None, reuse=None):
    """Initialize the cell.

    Args:
      damping_factor: damping the input hidden state impact along steps.
    """
    super(DampingLSTMCell, self).__init__(num_units,
                                          forget_bias=forget_bias,
                                          state_is_tuple=state_is_tuple,
                                          activation=activation,
                                          reuse=reuse)
    self._damping_factor = damping_factor

  def reset_step(self):
    self._curt_step = 0
    self._damping_weight = 1.

  def _inc_step(self):
    self._curt_step += 1
    self._damping_weight *= self._damping_factor

  def call(self, inputs, state):
    """Long short-term memory cell (LSTM).
    Args:
      inputs: `2-D` tensor with shape `[batch_size x input_size]`.
      state: An `LSTMStateTuple` of state tensors, each shaped
        `[batch_size x self.state_size]`, if `state_is_tuple` has been set to
        `True`.  Otherwise, a `Tensor` shaped
        `[batch_size x 2 * self.state_size]`.
    Returns:
      A pair containing the new hidden state, and the new state (either a
        `LSTMStateTuple` or a concatenated state, depending on
        `state_is_tuple`).
    """
    sigmoid = tf.nn.sigmoid
    # Parameters of gates are concatenated into one multiply for efficiency.
    if self._state_is_tuple:
      c, h = state
    else:
      c, h = tf.split(value=state, num_or_size_splits=2, axis=1)

    concat = _linear([inputs, h], 4 * self._num_units, True)

    # i = input_gate, j = new_input, f = forget_gate, o = output_gate
    i, j, f, o = tf.split(value=concat, num_or_size_splits=4, axis=1)

    print("encode_rnn", "step", self._curt_step, "weight", self._damping_weight)
    new_c = (
        c * sigmoid(f + self._forget_bias) + (
            self._damping_weight * sigmoid(i) * self._activation(j)))
    new_h = self._activation(new_c) # * sigmoid(o)

    if self._state_is_tuple:
      new_state = tf.contrib.rnn.LSTMStateTuple(new_c, new_h)
    else:
      new_state = tf.concat([new_c, new_h], 1)

    self._inc_step()

    return new_h, new_state


class LinearLSTMCell(tf.contrib.rnn.BasicLSTMCell):
  """
  """

  def __init__(self, num_units, damping_factor=1.0, forget_bias=1.0,
               state_is_tuple=True, activation=None, reuse=None):
    """Initialize the cell.

    Args:
      damping_factor: damping the input hidden state impact along steps.
    """
    super(LinearLSTMCell, self).__init__(num_units,
                                         forget_bias=forget_bias,
                                         state_is_tuple=state_is_tuple,
                                         activation=activation,
                                         reuse=reuse)
    self._damping_factor = damping_factor

  def reset_step(self):
    self._curt_step = 0
    self._damping_weight = 1.

  def _inc_step(self):
    self._curt_step += 1
    self._damping_weight *= self._damping_factor

  def __call__(self, inputs, state, scope=None):
    """Long short-term memory cell (LSTM).
    Args:
      inputs: `2-D` tensor with shape `[batch_size x input_size]`.
      state: An `LSTMStateTuple` of state tensors, each shaped
        `[batch_size x self.state_size]`, if `state_is_tuple` has been set to
        `True`.  Otherwise, a `Tensor` shaped
        `[batch_size x 2 * self.state_size]`.
    Returns:
      A pair containing the new hidden state, and the new state (either a
        `LSTMStateTuple` or a concatenated state, depending on
        `state_is_tuple`).
    """
    with tf.variable_scope(scope or "LinearLSTMCell"):
      # Parameters of gates are concatenated into one multiply for efficiency.
      if self._state_is_tuple:
        c, h = state
      else:
        c, h = tf.split(value=state, num_or_size_splits=2, axis=1)

      print("encode_rnn",
            "step",
            self._curt_step,
            "weight",
            self._damping_weight)

      input_state = _linear([inputs, h], 1 * self._num_units, True)
      new_c = c + self._activation(input_state)
      new_h = new_c
      """
      """

      """
      with tf.variable_scope("input_hidden_transform"):
        input_state = _linear([inputs, h], 1 * self._num_units, True)
        input_state = self._activation(input_state)
      with tf.variable_scope("input_combiner"):
        input_state = _linear(input_state, self._num_units, True)
        new_c = c + self._damping_weight * input_state
      new_h = new_c
      """

      """
      self._activation = tf.nn.relu
      with tf.variable_scope("input_transform"):
        inputs = tf.multiply(inputs, h)
        inputs = _linear(inputs, self._num_units, False)
        inputs = self._activation(inputs)
        h = self._activation(h)
      with tf.variable_scope("input_hidden_transform"):
        input_state = _linear([inputs, h], 1 * self._num_units, True)
        input_state = self._activation(input_state)
      with tf.variable_scope("input_combiner"):
        input_state = _linear(input_state, self._num_units, True)
        new_c = c + input_state
      new_h = new_c
      """

      """
      self._activation = tf.nn.relu
      with tf.variable_scope("input_transform"):
        h = tf.layers.batch_normalization(h)
        inputs = tf.multiply(inputs, h)
        inputs = _linear(inputs, self._num_units, True)
        inputs = tf.layers.batch_normalization(inputs)
        inputs = self._activation(inputs)
      with tf.variable_scope("input_hidden_transform"):
        input_state = _linear([inputs, h], 1 * self._num_units, True)
        input_state = tf.layers.batch_normalization(input_state)
        input_state = self._activation(input_state)
      with tf.variable_scope("input_combiner"):
        input_state = _linear(input_state, self._num_units, False)
        new_c = c + input_state
      new_h = self._activation(new_c)
      """

      """
      self._activation = tf.nn.relu
      with tf.variable_scope("input_transform"):
        h = tf.layers.batch_normalization(h)
        h = self._activation(h)
        inputs = tf.layers.batch_normalization(inputs)
        inputs = self._activation(inputs)
        input_state = _linear([inputs, h], 1 * self._num_units, True)
        input_state = tf.layers.batch_normalization(input_state)
        input_state = self._activation(input_state)
      with tf.variable_scope("input_combiner"):
        input_state = _linear(input_state, self._num_units, True)
        new_c = c + input_state
      new_h = new_c
      """

      if self._state_is_tuple:
        new_state = tf.contrib.rnn.LSTMStateTuple(new_c, new_h)
      else:
        new_state = tf.concat([new_c, new_h], 1)
      
      self._inc_step()

      return new_h, new_state


class BatchNormWrapper(tf.contrib.rnn.RNNCell):
  def __init__(self, cell, is_training, residual=False, in_keep_prob=1.0):
    self._cell = cell
    self._is_training = is_training
    self._residual = residual
    self._in_keep_prob = in_keep_prob

  @property
  def state_size(self):
    return self._cell.state_size

  @property
  def output_size(self):
    return self._cell.output_size

  def zero_state(self, batch_size, dtype):
    with tf.name_scope(type(self).__name__ + "ZeroState", values=[batch_size]):
      return self._cell.zero_state(batch_size, dtype)

  def __call__(self, inputs, state, scope=None):
    """Run the cell and then apply the residual_fn on its inputs to its outputs.
    Args:
      inputs: cell inputs.
      state: cell state.
      scope: optional cell scope.
    Returns:
      Tuple of cell outputs and new state.
    Raises:
      TypeError: If cell inputs and outputs have different structure (type).
      ValueError: If cell inputs and outputs have different structure (value).
    """
    inputs_b4_bn = inputs
    inputs = tf.layers.batch_normalization(inputs, training=self._is_training)
    if self._in_keep_prob < 1.0 and self._is_training:
      inputs = tf.nn.dropout(inputs, self._in_keep_prob)
    outputs, new_state = self._cell(inputs, state, scope=scope)
    if self._residual:
      outputs += inputs_b4_bn
    return (outputs, new_state)