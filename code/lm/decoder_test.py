"""Tests for decoder."""

import numpy as np
import tensorflow as tf
from decoder import Decoder


class DecoderTest(tf.test.TestCase):

  def setUp(self):
    pass

  def testScore(self):
    with tf.variable_scope("testScore", reuse=False):
      N, K, D, d = 10, 3, 5, 2
      outputs = np.random.random(N * d).reshape((-1, d)).astype("float32")
      decoder = Decoder(K, D, d, vocab_size=None, code=None)
      scores = decoder.score(outputs)

    with tf.Session() as sess:
      sess.run(tf.global_variables_initializer())
      scores = sess.run(scores)
      self.assertEqual(scores.shape, (N, D, K))

  def testOneHot(self):
    with tf.variable_scope("testOneHot", reuse=False):
      N, K, D, d = 10, 3, 5, 2
      decoder = Decoder(K, D, d, vocab_size=None, code=None)
      targets = np.random.randint(0, K, N * D).reshape((-1, D))
      targets_onehot = decoder.to_one_hot(targets, K=K)
      targets_toy = np.array([[0, 1], [3, 4]], dtype="int64")
      targets_toy_onehot = decoder.to_one_hot(targets_toy, K=6)

    with tf.Session() as sess:
      targets_onehot = sess.run(targets_onehot)
      self.assertEqual(targets_onehot.shape, (N, D, K))
      targets_toy_onehot = sess.run(targets_toy_onehot)
      targets_toy_onehot_true = np.array(
          [[[1, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0]],
          [[0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 1, 0]]])
      self.assertAllEqual(
          targets_toy_onehot.astype("int64"),
          targets_toy_onehot_true.astype("int64"))

  def testLoss(self):
    with tf.variable_scope("testLoss", reuse=False):
      N, K, D, d = 100, 5, 10, 3
      outputs = np.random.random(N * d).reshape((-1, d)).astype("float32")
      targets = np.random.randint(0, K, N * D).reshape((-1, D))
      decoder = Decoder(K, D, d, vocab_size=None, code=None)
      loss = decoder.loss(outputs, targets)

    with tf.Session() as sess:
      sess.run(tf.global_variables_initializer())
      loss = sess.run(loss)


if __name__ == "__main__":
  tf.test.main()
