import tensorflow as tf
from encoder import Encoder


_BIAS_VARIABLE_NAME = "bias"
_WEIGHTS_VARIABLE_NAME = "kernel"
BasicLSTMCell = tf.contrib.rnn.BasicLSTMCell
LSTMStateTuple = tf.contrib.rnn.LSTMStateTuple


class KDBasicLSTMCell(BasicLSTMCell):
  """ A KD factorized kernel of BasicLSTMCell.
  """

  def __init__(self, K, D, d, hparams, num_units, forget_bias=1.0,
               state_is_tuple=True, activation=None, reuse=None, name=None):
    """Initialize the cell.
    """
    super(KDBasicLSTMCell, self).__init__(num_units,
                                         forget_bias=forget_bias,
                                         state_is_tuple=state_is_tuple,
                                         activation=activation,
                                         reuse=reuse)
    self._K = K
    self._D = D
    self._d = d
    self._hparams = hparams

  def build(self, inputs_shape):
    if inputs_shape[1].value is None:
      raise ValueError("Expected inputs.shape[-1] to be known, saw shape: %s"
                       % inputs_shape)

    input_depth = inputs_shape[1].value
    h_depth = self._num_units

    # Re-parameterize.
    # shape=[input_depth + h_depth, 4 * self._num_units
    hparams = self._hparams
    self._pretrained_emb = None # DEBUG
    self._vocab_size = input_depth + h_depth
    self._encoder = Encoder(K=self._K,
                            D=self._D,
                            d=self._d,
                            outd=4 * self._num_units,
                            hparams=hparams,
                            vocab_size=self._vocab_size,
                            code_type=hparams.code_type,
                            code_emb_initializer=None,
                            emb_baseline=hparams.ec_emb_baseline,
                            create_code_logits=not hparams.ec_emb_autoencoding,
                            pretrained_emb=self._pretrained_emb)

    if hparams.ec_code_generator == "preassign":
        self._ec_is_one_hot = False
    else:
        self._ec_is_one_hot = True
    # self._kernel = self.add_variable(
    #    _WEIGHTS_VARIABLE_NAME,
    #    shape=[input_depth + h_depth, 4 * self._num_units])
    self._bias = self.add_variable(
        _BIAS_VARIABLE_NAME,
        shape=[4 * self._num_units],
        initializer=tf.zeros_initializer(dtype=self.dtype))

    self.built = True

  def call(self, inputs, state):
    """Long short-term memory cell (LSTM).
    Args:
      inputs: `2-D` tensor with shape `[batch_size, input_size]`.
      state: An `LSTMStateTuple` of state tensors, each shaped
        `[batch_size, self.state_size]`, if `state_is_tuple` has been set to
        `True`.  Otherwise, a `Tensor` shaped
        `[batch_size, 2 * self.state_size]`.
    Returns:
      A pair containing the new hidden state, and the new state (either a
        `LSTMStateTuple` or a concatenated state, depending on
        `state_is_tuple`).
    """
    sigmoid = tf.sigmoid
    one = tf.constant(1, dtype=tf.int32)
    # Parameters of gates are concatenated into one multiply for efficiency.
    if self._state_is_tuple:
      c, h = state
    else:
      c, h = tf.split(value=state, num_or_size_splits=2, axis=one)

    is_training = True  # DEBUG, add is_training or remove BN etc.
    if self._hparams.ec_emb_baseline:
        code_logits = None
        if self._hparams.ec_emb_autoencoding:
            _embs = tf.nn.embedding_lookup(
                self._pretrained_emb, tf.range(vocab_size))
            code_logits = self._encoder.embed_transpose(
                _embs, is_training=is_training)
        codes, code_embs, embsb = self._encoder.symbol2code(
                tf.range(vocab_size),
                is_training=is_training,
                logits=code_logits,
                output_embb=True)
    else:
        embsb = None
        codes, code_embs = self._encoder.symbol2code(
                tf.range(self._vocab_size), is_training=is_training)
    embs = self._encoder.embed(
            codes, code_embs=code_embs, embsb=embsb,
            is_one_hot=self._ec_is_one_hot, is_training=is_training)
    gate_inputs = tf.matmul(
        tf.concat([inputs, h], 1), embs)
    #gate_inputs = tf.matmul(
    #    tf.concat([inputs, h], 1), self._kernel)
    gate_inputs = tf.nn.bias_add(gate_inputs, self._bias)

    # i = input_gate, j = new_input, f = forget_gate, o = output_gate
    i, j, f, o = tf.split(
        value=gate_inputs, num_or_size_splits=4, axis=one)

    forget_bias_tensor = tf.constant(self._forget_bias, dtype=f.dtype)
    # Note that using `add` and `multiply` instead of `+` and `*` gives a
    # performance improvement. So using those at the cost of readability.
    add = tf.add
    multiply = tf.multiply
    new_c = add(multiply(c, sigmoid(add(f, forget_bias_tensor))),
                multiply(sigmoid(i), self._activation(j)))
    new_h = multiply(self._activation(new_c), sigmoid(o))

    if self._state_is_tuple:
      new_state = LSTMStateTuple(new_c, new_h)
    else:
      new_state = tf.concat([new_c, new_h], 1)
    return new_h, new_state
