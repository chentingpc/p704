"""Tests for encoder."""

import tensorflow as tf
from encoder import Encoder


class EncoderTest(tf.test.TestCase):

  def setUp(self):
    pass

  def testCompactCode(self):
    with tf.variable_scope("testCompactCode", reuse=False):
      encoder = Encoder(K=3, D=5, d=10, vocab_size=100, code_type="compact")
    with tf.Session() as sess:
      sess.run(tf.global_variables_initializer())
      code = sess.run(encoder.code)
      self.assertEqual(code.shape, (100, 5))
      self.assertEqual(list(code[0]), [0, 0, 0, 0, 0])
      self.assertEqual(list(code[10]), [0, 0, 1, 0, 1])
      self.assertEqual(list(code[81]), [1, 0, 0, 0, 0])

  def testRedundantCode(self):
    with tf.variable_scope("testRedundantCode", reuse=False):
      encoder = Encoder(K=3, D=10, d=10, vocab_size=100, code_type="redundant")
    with tf.Session() as sess:
      sess.run(tf.global_variables_initializer())
      code = sess.run(encoder.code)
      self.assertEqual(code.shape, (100, 10))
      # The collisions should be small.
      self.assertGreater(set([tuple(c) for c in code.tolist()]), 90)

  def testCompactEncode(self):
    with tf.variable_scope("testCompactEncode", reuse=False):
      encoder = Encoder(K=3, D=5, d=10, vocab_size=100, code_type="compact")
      inputs = tf.convert_to_tensor([[1, 2], [3, 4]])
      input_shape = inputs.shape.as_list()
      codes = encoder.symbol2code(inputs)
      codes_shape = codes.shape.as_list()
      self.assertEqual(input_shape + [5], codes_shape)
      embs = encoder.encode(inputs)
      embs_shape = embs.shape.as_list()
      self.assertEqual(input_shape + [10], embs_shape)

  def testRedundantEncode(self):
    with tf.variable_scope("testRedundantEncode", reuse=False):
      encoder = Encoder(K=3, D=5, d=10, vocab_size=100, code_type="redundant")
      inputs = tf.convert_to_tensor([[1, 2], [3, 4]])
      input_shape = inputs.shape.as_list()
      codes = encoder.symbol2code(inputs)
      codes_shape = codes.shape.as_list()
      self.assertEqual(input_shape + [5], codes_shape)
      embs = encoder.encode(inputs)
      embs_shape = embs.shape.as_list()
      self.assertEqual(input_shape + [10], embs_shape)


if __name__ == "__main__":
  tf.test.main()
