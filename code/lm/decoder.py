"""Decoder class that maps continous vector to discrete symbol"""

import tensorflow as tf
import util
from coder import Coder


class Decoder(Coder):

  def __init__(self,
               K,
               D,
               d,
               hparams,
               vocab_size,
               code,
               code_embedding=None,
               code_emb_intializer=None):
    self._K = K
    self._D = D
    self._d = d
    self._hparams = hparams
    self._vocab_size = vocab_size
    self._code = code
    self._code_emb = code_embedding
    self._code_emb_intializer = code_emb_intializer

  def score(self, outputs, targets=None, is_one_hot=False, hparams=None):
    """Score the outputs for all dimensions of discrete codes.
    
    Args:
      outputs: A `Tensor` of size (M, d) where M is the number of instances.
      targets: A `Tensor` of size (M, D) where M is the number of instances, and
        D is the discrete code size. When is_one_hot=True, this tensor is in 
        one-hot encoding format which has shape (M, D, K). This is only used for
        certain scorer.
      is_one_hot: A `Bool` specifying whether the targets are in index format or
        one-hot format.
  
    Returns:
      A score `Tensor` of size (M, D, K)
    """
    if hparams is None:
      hparams = self._hparams
    get_hparam = util.hparam_fn(hparams, prefix="dc")
    get_actv = util.filter_activation_fn()

    shared_coding = get_hparam("shared_coding")
    scorer = get_hparam("scorer")
    fnn_hidden_size = get_actv(get_hparam("fnn_hidden_size"))
    fnn_hidden_actv = get_actv(get_hparam("fnn_hidden_actv"))

    if self._code_emb is None:
      reuse = False
    else:
      reuse = True

    with tf.variable_scope("decode_score", reuse=reuse):
      if scorer == "fnn":
        scores = tf.layers.dense(outputs,
                                 fnn_hidden_size,
                                 activation=fnn_hidden_actv)
        scores = tf.layers.dense(scores, self._D * self._K, use_bias=False)
        scores = tf.reshape(scores, [-1, self._D, self._K])
      elif scorer == "cnn":
        scores = tf.layers.dense(outputs, self._D * self._d)
        scores = tf.reshape(scores, [-1, self._D, self._d])
        scores = self._spatial_function(
            scores,
            function_name="cnn",
            strict_direction=False,
            hparams=hparams)
      elif scorer == "rnn":
        outputs = tf.expand_dims(outputs, 1)
        scores = tf.tile(outputs, [1, self._D, 1])
        scores = self._spatial_function(
            scores,
            function_name="rnn",
            strict_direction=False,
            hparams=hparams)
      elif scorer == "hsm":
        base = [1]
        for i in range(self._D - 1):
          base.append(base[-1] * self._K)
        prefix_cnt = sum(base)
        # prefix_cnt = reduce(lambda x, y: x * self._K + 1, range(1, self._D))
        self._code_emb = tf.get_variable(
            "code_embedding",
            [prefix_cnt, self._K * self._d],
            initializer=self._code_emb_intializer,
            dtype=tf.float32)

        # Use true label prefix to find code embedding.
        base_tensor = tf.constant([0] + base[1:-1])
        if is_one_hot:
          targets = tf.cast(tf.argmax(targets, -1), tf.int32)
        prefix = targets[:, :-1]  + base_tensor # (M, D - 1)
        prefix = tf.concat(  # becoming (M, D)
            [tf.zeros_like(targets[:, :1]) + (prefix_cnt - 1), prefix], -1)
        prefix_embs = tf.nn.embedding_lookup(self._code_emb, prefix)
        prefix_embs = tf.reshape(
            prefix_embs, [-1, self._D, self._K, self._d])

        # Scoring the likelihood of each given prefix_embs.
        if outputs.shape[1].value != self._d:
          outputs = tf.layers.dense(outputs, self._d, use_bias=False)
        outputs = tf.reshape(outputs, [-1, 1, self._d, 1])
        outputs = tf.tile(outputs, [1, self._D, 1, 1])  # (M, D, d, 1])
        scores = tf.matmul(prefix_embs, outputs)
        scores = tf.squeeze(scores)
      elif scorer.startswith("dependent"):
        if is_one_hot:
          codes = targets[:, :-1, :]
          if shared_coding:
            code_embedding_size = [self._K]
          else:
            code_embedding_size = [self._D, self._K]
        else:
          codes = targets[:, :-1]
          if shared_coding:
            code_embedding_size = [self._K]
            codes_shifted = codes
          else:
            code_embedding_size = [self._D * self._K]
            shifts = tf.reshape(
                tf.range(self._D-1) * self._K,
                [1] * (len(codes.shape.as_list()) - 1) + [-1])
            # Move codes to same idx space for the ease of parameterization.
            codes_shifted = codes + shifts

        self._code_emb = tf.get_variable(
            "code_embedding",
            code_embedding_size + [self._d],
            initializer=self._code_emb_intializer,
              dtype=tf.float32)

        # Map codes to code embeddings, (M, d).
        if is_one_hot:
          if shared_coding:
            target_embs = tf.matmul(codes, self._code_emb)
          else:
            # TODO: more efficient implementation than tile.
            # codes = tf.reshape(codes, [-1, self._D, 1, self._K])  # (M, D, 1, K)
            codes = tf.expand_dims(codes, -2)  # (M, D-1, 1, K)
            code_emb = self._code_emb[:-1, :, :]
            code_emb = tf.reshape(
                code_emb,
                [1, self._D - 1, self._K, self._d])
            code_emb = tf.tile(
                code_emb, tf.concat([tf.shape(codes)[:-3], [1, 1, 1]], 0))
            target_embs = tf.matmul(codes, code_emb)
            target_embs = tf.squeeze(target_embs, [-2])
        else:
          target_embs = tf.nn.embedding_lookup(
              self._code_emb, codes_shifted)

        # Generate (D, d) embedding input for the decoding network,
        # (0, :) vector is the output state, (1:, :) vectors are target codes
        # from previous step.
        if outputs.shape[1].value != self._d:
          outputs = tf.layers.dense(outputs, self._d, use_bias=False)
        outputs = tf.expand_dims(outputs, 1)
        inputs_t = tf.concat([outputs, target_embs], 1) # (M, D, d)

        if scorer == "dependent_rnn":
          scores = self._spatial_function(
              inputs_t,
              function_name="rnn",
              strict_direction=True,
              hparams=hparams)
        elif scorer == "dependent_cnn":
          scores = self._spatial_function(
              inputs_t,
              function_name="cnn",
              strict_direction=True,
              hparams=hparams)
        else:
          raise ValueError("Unknown scorer {}".foramt(scorer))
      else:
        raise ValueError("Unknown scorer {}".foramt(scorer))
    
    return scores

  def _spatial_function(self,
                        inputs,
                        function_name="rnn",
                        strict_direction=True,
                        hparams=None):
    """Define and apply a spatial function on inputs.

    The function performs aggregation over time dimensions, and output the 
    probability over K-way code positions.

    Args:
      inputs: a `Tensor` of (M, D, d') where M is instance size, D is the
        discrete code size (also the number of time steps), d' is the
        latent dimensions
      function_name: A `String` name: "rnn", "cnn".
      strict_direction: a `Bool`. When set True, the spatial model at time t can
        also use inputs at time t-1 or before; otherwise not such constraints.

    Returns:
      A `Tensor` of shape (M, D, K).
    """
    if hparams is None:
      hparams = self._hparams
    get_hparam = util.hparam_fn(hparams, prefix="dc")
    get_actv = util.filter_activation_fn()

    cnn_filters = get_hparam("cnn_filters")
    cnn_kernel_size = get_hparam("cnn_kernel_size")
    cnn_hidden_actv = get_actv(get_hparam("cnn_hidden_actv"))
    rnn_num_layers = get_hparam("rnn_num_layers")
    rnn_hidden_size = get_hparam("rnn_hidden_size")

    if function_name == "rnn":
      cell = tf.contrib.rnn.LSTMBlockCell(
          rnn_hidden_size, forget_bias=0.0)
      cell = tf.contrib.rnn.MultiRNNCell(
          [cell for _ in range(rnn_num_layers)], state_is_tuple=True)
      # initial_state = cell.zero_state(tf.shape(inputs)[0], "float32")

      # Compute and generate (M, D, rnn_hidden_size).
      if strict_direction:
        inputs = tf.unstack(inputs, num=self._D, axis=1)
        scores, state = tf.contrib.rnn.static_rnn(
            cell, inputs, dtype=tf.float32)
        scores = [tf.expand_dims(score, 1) for score in scores]
        scores = tf.concat(scores, 1)
      else:
        inputs = tf.unstack(inputs, num=self._D, axis=1)
        scores, _, _  = tf.contrib.rnn.static_bidirectional_rnn(
            cell, cell, inputs, dtype=tf.float32)
        scores = [tf.expand_dims(score, 1) for score in scores]
        scores = tf.concat(scores, 1)  # (M, D, rnn_hidden_size)

      # (M, D, K)
      scores = tf.layers.dense(scores, self._K, use_bias=False)
    elif function_name == "cnn":

      # Compute and generate (M, D, cnn_filters).
      if strict_direction:
        # TODO: change kernel to only look backward but not forward.
        raise NotImplemented()
      else:
        # (M, D, filters)
        scores = tf.layers.conv1d(inputs,
                                  filters=cnn_filters,
                                  kernel_size=cnn_kernel_size,
                                  padding="same",
                                  data_format='channels_last',
                                  activation=cnn_hidden_actv)
      # (M, D, K)
      scores = tf.layers.dense(scores, self._K, use_bias=False)
    else:
      raise ValueError("Unknown function name {}".format(function_name))

    return scores

  def to_one_hot(self, targets, K=None):
    """Map targets of symbols to one-hot representation.

    Args:
      targets: a target symbol `Tensor` of shape (M, D) where M is the number of
        instances and D is the discrete code size.
      K: a `int` number specifying the size of one-hot vector. If not set, will
        be set to self._K.

    Returns:
      A one-hot representation `Tensor` of shape (M, D, vocab)
    """
    if K is None:
      K = self._K
    return tf.one_hot(targets,
                      depth=K,
                      on_value=1.0,
                      off_value=0.0,
                      axis=-1)

  def loss(self, outputs, targets, is_one_hot=False, hparams=None):
    """Compute the loss given hidden vector outputs, and discrete code targets.

    Args:
      outputs: a `Tensor` of size (M, d) where M is the number of instances and
        d is the hidden vector / embedding size.
      targets: a `Tensor` of size (M, D) where M is the number of instances and
        D is the discrete code size. When is_one_hot=True, this tensor shape
        will be (M, D, K).
      is_one_hot: a `Bool` specifying the target is in index format or one-hot
        format.

    Returns:
      a loss `Tensor`.
    """
    scores = self.score(
        outputs, targets=targets, is_one_hot=is_one_hot)  # (M, D, K)
    if is_one_hot:
      loss = -tf.reduce_sum(targets * util.safer_log(tf.nn.softmax(scores)))
      # Warning: softmax_cross_entropy_with_logits does not BP to labels.
      # loss = tf.nn.softmax_cross_entropy_with_logits(
      #    labels=targets, logits=scores)
    else:
      targets = self.to_one_hot(targets)
      loss = -tf.reduce_sum(targets * util.safer_log(tf.nn.softmax(scores)))
      # Warning: sparse_softmax_cross_entropy_with_logits does not BP to labels.
      # loss = tf.nn.sparse_softmax_cross_entropy_with_logits(
      #     labels=targets, logits=scores)
    return loss


  def decode(self, outputs, top_k):
    """Decode top k most likely symbols given outputs.

    Args:
      outputs: a `Tensor` of shape (M, d) where M is the number of instances and
        d is the hindden vector /embedding size.
      top_k: a 'int' specifying the top k symbols to return.

    Returns:
      A `Tensor` of shape (M, 1) where each element is a symbol.
    """
    scores = self.score(outputs)  # (M, D, K)
    # top_codes = cpp_decode_op(scores, top_k)
    raise NotImplemented()
