import gumbel as gb
import util
import tensorflow as tf


class VectorQuantization(object):
  def __init__(self, K, D, d, shared_codebook=False):
    self._K = K
    self._D = D
    self._d = d

    with tf.variable_scope("VQ"):
      if shared_codebook:
        codebook = tf.get_variable("codebook", [1, K, d])
        codebook = tf.tile(codebook, [D, 1, 1])
      else:
        codebook = tf.get_variable("codebook", [D, K, d])
      self._codebook = codebook

  def forward(self,
              embs,
              emb_normalization=True,
              sampling=False,
              is_training=False):
    """Returns quantized embeddings from code book and regularization cost.

    Args:
      embs: embedding tensor of shape (batch_size, D, d)

    Returns:
      embs_quantized, code, regularization
    """
    with tf.name_scope("vq_forward"):
      # House keeping.
      codebook = self._codebook
      if emb_normalization:
        embs = tf.nn.l2_normalize(embs, -1)
        codebook = tf.nn.l2_normalize(codebook, -1)
      codebook_bsize = tf.tile(
          tf.expand_dims(codebook, 0),
          [tf.shape(embs)[0], 1, 1, 1])  # (batch_size, D, K, d)
      codebook_bsize_t = tf.transpose(codebook_bsize, [0, 1, 3, 2])  # (batch_size, D, d, K)

      # Compute l2-distance and get/sample nearest neighbor.
      if emb_normalization:
        response = tf.matmul(
            codebook_bsize, tf.expand_dims(embs, -1))  # (batch_size, D, K, 1)
      else:
        response = tf.matmul(
            tf.nn.l2_normalize(codebook_bsize, -1),
            tf.expand_dims(tf.nn.l2_normalize(embs, -1), -1))  # (batch_size, D, K, 1)
      codes = tf.argmax(tf.squeeze(response, -1), -1)  # (batch_size, D)
      if sampling:
        response = util.safer_log(tf.nn.softmax(tf.squeeze(response, -1), -1))
        noises = gb.sample_gumbel(tf.shape(response))
        neighbor_idxs = tf.argmax(response + noises, -1)  # (batch_size, D)
      else:
        neighbor_idxs = codes
      indicators = tf.stop_gradient(
          tf.expand_dims(tf.one_hot(neighbor_idxs, self._K), -1))  # (batch_size, D, K, 1)

      # Compute the output embedding.
      embs_q = tf.squeeze(tf.matmul(codebook_bsize_t, indicators), -1)  # (batch_size, D, d)
      embs_out = tf.stop_gradient(embs_q - embs) + embs

      if is_training:
        # beta = 0.25
        beta = 1.
        vq_reg = tf.reduce_mean(
            tf.reduce_sum(tf.square(embs_q - tf.stop_gradient(embs)), [1, 2]),
            name="emb")
        vq_reg += beta * tf.reduce_mean(
            tf.reduce_sum(tf.square(tf.stop_gradient(embs_q) - embs), [1, 2]),
            name="commit")
        tf.add_to_collection(tf.GraphKeys.REGULARIZATION_LOSSES, vq_reg)

      return codes, embs_out


if __name__ == "__main__":
  vq = VectorQuantization(100, 10, 5)
  embs_q, l_reg = vq.forward(tf.random_normal([64, 10, 5]))