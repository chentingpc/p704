#!/bin/bash
#########################################################################
# This script is used to generate full embedding and low-rank baselines.
# Options:
#   save_embedding: to save the pre-trained embedding when using
#     full embedding baseline, set it to true.
#   emb_lowrank_dim: to enable low-rank baseline, set emb_lowrank_dim
#     to any float number between 0 and 1, denoting keep params rate.
#########################################################################
cd ..
gpu=$1
echo GPU=$gpu

use_recoding=False
dataset=ptb
#dataset=text8
data_path=~/dbase/corpus/language_model/$dataset/original/
save_root=~/pbase/x/p704/results/temp/
save_embedding=false
#rnn_mode=block
rnn_mode=basic
optimizer=sgd
ec_logits_bn=1.0

# 32,16 16,16 16,32
for KD in 16,32; do
#for KD in 32,32; do
IFS=","; set -- $KD; K=$1; D=$2;
for compound in none,1000,1,1e-2; do
IFS=","; set -- $compound; method=$1; step=$2; rate=$3; bound=$4
#for ec_code_emb_dim in 800; do
for ec_code_emb_dim in 2600; do
#for ec_code_emb_dim in 6000; do
for ec_code_generator in STE_argmax ; do
#for ec_code_generator in preassign STE_argmax gumbel_softmax; do
for learning_rate in 1 ; do
for optimizer in sgd ; do
#for optimizer in lazy_adam sgd scheduled_sgd momentum; do
for ec_aggregator in mean ; do
#for ec_aggregator in mean mean_fnn rnn; do
for ec_emb_baseline in False ; do
for ec_emb_baseline_reg in 0; do
for ec_entropy_reg in 0; do

for model in medium; do
#for model in small medium large; do
for emb_lowrank_dim in 0; do
	if [ $save_embedding == true ]; then
		emb_save_filename=~/pbase/x/p704/pretrains/lm/$model\_embs.pkl
		echo "Will save embedding to $emb_save_filename"
	else
		echo "Won't save embedding"
	fi
	if [ $model == small ]; then
		max_max_epoch=15
		max_grad_norm=5
	elif [ $model == medium ]; then
		max_max_epoch=30
		max_grad_norm=5
	else
		max_max_epoch=35
		if [ $emb_lowrank_dim != 0 ]; then
			max_grad_norm=5
		else
			max_grad_norm=10
		fi
	fi
	save_path=$save_root/$(date +"%m%d_%H%M%S")\_model$model\_rnnmode$rnn_mode\_optimizer$optimizer\_maxgradn$max_grad_norm\_lowrankd$emb_lowrank_dim
	echo $save_path
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python ptb_word_lm.py --dataset=$dataset --data_path=$data_path --model=$model --use_recoding=$use_recoding --rnn_mode=$rnn_mode --optimizer=$optimizer --save_path=$save_path --max_max_epoch=$max_max_epoch --max_grad_norm=$max_grad_norm --emb_save_filename=$emb_save_filename --emb_lowrank_dim=$emb_lowrank_dim --K=$K --D=$D --ec_code_emb_dim=$ec_code_emb_dim --ec_code_generator=$ec_code_generator --ec_aggregator=$ec_aggregator --ec_logits_bn=$ec_logits_bn >$save_path/log 2>&1
done
done
done
done
done
done
done
done
done
done
done
done
