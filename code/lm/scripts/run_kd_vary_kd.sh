#!/bin/bash
#############################################################################
# This script is used to generate results under different choices of K and D.
# Options:
#   K, D: the KD used for code size.
#   ec_aggregator: either mean or rnn will be compared.
# Any other options should be leaved as default.
#############################################################################
cd ..
gpu=$1
echo $gpu

#model=small
#model=medium
model=large

#run_mode=random_code
#run_mode=pretr_code
#run_mode=no_guide
#run_mode=online_guide
run_mode=emb_guide

dataset=ptb
#dataset=text8
data_path=~/dbase/corpus/language_model/$dataset/original/
save_root=~/pbase/x/p704/results/temp/
rnn_mode=block
use_recoding=True
code_type=redundant
rnn_residual=False
ec_hard_code_output=True
ec_STE_softmax_transform=True
ec_logits_bn=1.0
ec_code_dropout=0.
emb_baseline_dropout=0.
ec_emb_autoencoding=False
ec_emb_transpose_layers=1
ec_emb_transpose_dim=200
ec_emb_transpose_actv=relu

if [ $model == small ]; then
	max_max_epoch=15
	max_grad_norm=3
else
	max_max_epoch=35
	max_grad_norm=5
fi

for K in 4 8 16 32 64; do
for D in 4 8 16 32 64; do
for compound in none,1000,1,1e-2; do
IFS=","; set -- $compound; method=$1; step=$2; rate=$3; bound=$4
for emb_dim in 300 ; do
#for emb_dim in 100 200 300; do
for ec_code_generator in STE_argmax ; do
#for ec_code_generator in preassign STE_argmax gumbel_softmax; do
for learning_rate in 1 ; do
for optimizer in mixed ; do
#for optimizer in lazy_adam sgd scheduled_sgd momentum; do
for ec_aggregator in mean rnn; do
for ec_emb_baseline in False ; do
for ec_emb_baseline_reg in 0; do
for ec_entropy_reg in 0; do
	ec_code_emb_dim=$emb_dim
	ec_fnn_hidden_size=300
	ec_fnn_hidden_actv=linear
	ec_rnn_num_layers=1
	ec_rnn_bidirection=False
	#ec_rnn_hidden_size=300
	ec_rnn_hidden_size=500
	ec_shared_coding=False
	ec_temperature_decay_method=$method
	ec_temperature_decay_steps=$step
	ec_temperature_decay_rate=$rate
	ec_temperature_lower_bound=$bound

	code_load_filename=
	emb_load_filename=
	if [ $run_mode == random_code ]; then
		ec_code_generator=preassign
		optimizer=scheduled_sgd
	elif [ $run_mode == pretr_code ]; then
		code_load_filename=~/pbase/x/p704/pretrains/lm/$model\_embs.code.K$K\D$D.pkl
		ec_code_generator=preassign
		optimizer=scheduled_sgd
	elif [ $run_mode == online_guide ]; then
		ec_emb_baseline=True
		ec_emb_baseline_reg=0.1
		emb_baseline_dropout=0.5
	elif [ $run_mode == emb_guide ]; then
		ec_emb_baseline=True
		ec_emb_baseline_reg=1000
		emb_load_filename=~/pbase/x/p704/pretrains/lm/$model\_embs.pkl
	fi

	save_path=$save_root/$(date +"%m%d_%H%M%S")\_m$model\_r$run_mode\_K$K\_D$D\_cg$ec_code_generator\_cshare$ec_shared_coding\_hard$ec_hard_code_output\_logitsbn$ec_logits_bn\_aggregator$ec_aggregator\_optimizer$optimizer\_dim$emb_dim\_decay$method\_decaystep$step\_decayrate$rate\_lr$learning_rate\_maxgradn$max_grad_norm\_basereg$ec_emb_baseline_reg\_entreg$ec_entropy_reg
	echo $save_path
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python ptb_word_lm.py --dataset=$dataset --data_path=$data_path --model=$model --use_recoding=$use_recoding --rnn_mode=$rnn_mode --optimizer=$optimizer --save_path=$save_path --max_max_epoch=$max_max_epoch --max_grad_norm=$max_grad_norm --K=$K --D=$D --ec_logits_bn=$ec_logits_bn --ec_rnn_num_layers=$ec_rnn_num_layers --ec_rnn_bidirection=$ec_rnn_bidirection --ec_code_generator=$ec_code_generator --code_type=$code_type --ec_STE_softmax_transform=$ec_STE_softmax_transform --ec_hard_code_output=$ec_hard_code_output --ec_aggregator=$ec_aggregator --ec_code_emb_dim=$ec_code_emb_dim --ec_rnn_hidden_size=$ec_rnn_hidden_size --ec_shared_coding=$ec_shared_coding --ec_temperature_decay_method=$ec_temperature_decay_method --ec_temperature_decay_steps=$ec_temperature_decay_steps --ec_temperature_decay_rate=$ec_temperature_decay_rate --code_load_filename=$code_load_filename --rnn_residual=$rnn_residual --learning_rate=$learning_rate --ec_code_dropout=$ec_code_dropout --ec_emb_baseline=$ec_emb_baseline --ec_emb_baseline_reg=$ec_emb_baseline_reg --emb_load_filename=$emb_load_filename --ec_entropy_reg=$ec_entropy_reg --ec_emb_autoencoding=$ec_emb_autoencoding --ec_temperature_lower_bound=$ec_temperature_lower_bound --emb_baseline_dropout=$emb_baseline_dropout >$save_path/log 2>&1 #&
	#let "gpu+=1"
done
done
done
done
done
done
done
done
done
done
done
