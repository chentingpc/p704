import tensorflow as tf
from encoder import Encoder


class KDDense(object):
  """A KD based Dense transformation parameterization (d_in, d_out).
  """
  def __init__(self, K, D, d, hparams, d_in, d_out, scheme="pq", name=None):
    """Initialization.

    Args:
      scheme: "vq" or "pq". "vq" stands for vector quantization style KD code
        based compression; "pq" stands for learning to prune and (scalar-style)
        quantization.
    """
    self._K = K
    self._D = D
    self._d = d
    self._hparams = hparams
    self._d_in = d_in
    self._d_out = d_out
    self._scheme = scheme
    self._name = name

    assert scheme in ["vq", "pq"]

    self._build()

  def _build(self):
    hparams = self._hparams
    if self._scheme == "vq":
      self._encoder = Encoder(K=self._K,
                              D=self._D,
                              d=self._d,
                              outd=self._d_out,
                              hparams=hparams,
                              vocab_size=self._d_in,
                              code_type=hparams.code_type,
                              code_emb_initializer=None,
                              emb_baseline=hparams.ec_emb_baseline,
                              create_code_logits=not hparams.ec_emb_autoencoding,
                              pretrained_emb=None)
      if hparams.ec_code_generator == "preassign":
          self._ec_is_one_hot = False
      else:
          self._ec_is_one_hot = True
    elif self._scheme == "pq":
      # TODO

  def get_weight_matrix(self):
    """Returns the pruned and/or quantized dense weight matrix.
    """
    hparams = self._hparams
    if self._scheme == "vq":
      is_training = True  # DEBUG, add is_training or remove BN etc.
      if hparams.ec_emb_baseline:
        code_logits = None
        if hparams.ec_emb_autoencoding:
          _embs = tf.nn.embedding_lookup(
            self._pretrained_emb, tf.range(vocab_size))
          code_logits = self._encoder.embed_transpose(
            _embs, is_training=is_training)
        codes, code_embs, embsb = self._encoder.symbol2code(
            tf.range(vocab_size),
            is_training=is_training,
            logits=code_logits,
            output_embb=True)
      else:
        embsb = None
        codes, code_embs = self._encoder.symbol2code(
          tf.range(self._d_in), is_training=is_training)
      W = self._encoder.embed(
          codes, code_embs=code_embs, embsb=embsb,
          is_one_hot=self._ec_is_one_hot, is_training=is_training)
    else self._scheme == "pq":
      # TODO
    return W
