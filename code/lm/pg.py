"""Mirrored from p705, check there for consistency.
Modifications: (1) relax_loss_p.
"""
import itertools
import numpy as np
import tensorflow as tf
import gumbel as gb
import util
safer_log = util.safer_log


class PolicyGradient(object):
  def __init__(self,
               K,
               D,
               hparams,
               num_samples=1,
               one_hot_action=True,
               theta_params=None):
    """ The Policy Gradient helper class (only support discrete KD variables).

    In the forward function run, logits are passed in and actions are returned.
    The external program takes the actions and evaluate their scores, i.e. f(b). 
    In the backward function run, scores passed in and update ops are computed.

    Note that when setting num_samples<0, the first _first_n4f samples are
    drawn for training of f, and the remaining are used for policy training.

    Args:
      K: a `int` of the number of categorical categories.
      D: a `int` of the number of categorical variables.
      num_samples: non-positive number means computing each of the K actions.
      one_hot_action: whether or not to use the one-hot representation for 
        action outputs.
    """
    self._K = K
    self._D = D
    self._hparams = hparams
    self._num_samples = num_samples
    self._one_hot_action = one_hot_action
    self._theta_params = theta_params
    self._optimizer = None

  def forward(self, logits, action_prob=True, is_training=False):
    """
    Args:
      logits: (batch_size, D, K) logits.
      action_prob: if True, also return one-hot vector weighted by probability,
        i.e. 0 still zero, but non-zero weighted by probability. 

    Returns:
      (num_samples, batch_size, D, K) where each of D-dimension is one-hot when
        one_hot_action=True.
      (num_samples, batch_size, D) where each of D-dimension is an integer when
        one_hot_action=False.

    Remarks:
      It should also work if batch_size is a *list instead of a number.
    """
    p = tf.nn.softmax(logits, -1)  # Probability.
    if is_training or self._hparams.test_summary:
      tf.summary.histogram("p", p)
    logp = safer_log(p)
    if self._num_samples <= 0:
      combinations = tf.constant(np.array(
          [each for each in itertools.product(*[range(self._K)] * self._D)]))
      actions4p = tf.tile(tf.expand_dims(combinations, 1),
                        [1, tf.shape(logits)[0], 1])
      first_n4f = -self._num_samples if self._num_samples < 0 else 1
      actions4f = self._sample(logp, first_n4f)
      actions_idx = tf.concat([actions4f, actions4p], 0)
    else:
      first_n4f = None
      #actions_idx = self._sample(logits, self._num_samples)
      actions_idx = self._sample(logp, self._num_samples)
    actions_onehot = tf.one_hot(actions_idx, self._K)
    actions = actions_onehot if self._one_hot_action else actions_idx

    action_probs_onehot = actions_onehot * p
    action_probs = tf.reduce_prod(
        tf.reduce_sum(action_probs_onehot, -1), -1)  #(num_samples, batch_size)
    if is_training or self._hparams.test_summary:
      if self._num_samples <= 0:
        ap = action_probs[first_n4f:]
        ap_entropy = - tf.reduce_mean(tf.reduce_sum(ap * safer_log(ap), 0))
      else:
        ap = action_probs
        ap_entropy = - tf.reduce_mean(tf.reduce_sum(safer_log(ap), 0))
      tf.summary.scalar("ap_entropy", ap_entropy)
    
    if is_training:
      # Saved variable for backward use.
      self._p = p
      self._logp = logp
      self._actions_idx = actions_idx
      self._first_n4f = first_n4f
      self._actions_onehot = actions_onehot
      self._actions = actions
      self._action_probs = action_probs
      self._action_probs_onehot = action_probs_onehot

    if action_prob:
      if not self._one_hot_action: raise ValueError()
      return actions, action_probs_onehot
    else:
      return actions

  def backward(self, scores, theta_params=None):
    """When *training*, call this to get the train_op for the policy.

    Args:
      scores: f(b) evaluation of size (num_samples, batch_size).

    Returns:
      Training ops.

    Remarks:
      It should also work if batch_size is a *list instead of a number.
    """
    optimizer = self._hparams.policy_optimizer
    learning_rate = self._hparams.policy_learning_rate
    if self._one_hot_action:
      actions = self._actions
    else:
      actions = tf.one_hot(self._actions, self._K)
    if self._num_samples <= 0:
      scores, actions = scores[self._first_n4f:], actions[self._first_n4f:]
      action_probs = self._action_probs[self._first_n4f:]
    if theta_params is None:
      theta_params = self._theta_params
    """ DEBUG: power up p trick.
    init = 1.
    decay_steps = 1000
    decay_rate = 1
    p_temp = tf.get_variable("p_temp", initializer=init, dtype=tf.float32, trainable=False)
    decayer = tf.train.exponential_decay
    decayer = tf.train.inverse_time_decay
    p_temp = decayer(
        p_temp,
        tf.train.get_or_create_global_step(),
        decay_steps,
        decay_rate,
        staircase=True)
    p_temp = tf.clip_by_value(p_temp, 1., 1.0)
    #p_temp = tf.get_variable("p_temp", [], initializer=tf.ones_initializer)
    tf.summary.scalar("p_temp", p_temp)
    pp = tf.pow(tf.stop_gradient(self._p), p_temp)
    """
    #pp = tf.stop_gradient(self._p)
    #logp = tf.expand_dims(pp * self._logp, 0)  # (1, batch_size, D, K)  # DEBUG
    logp = tf.expand_dims(self._logp, 0)  # (1, batch_size, D, K)
    logp_selected = tf.reduce_sum(  # prod(p) == sum(logp) forall each in D.
        tf.multiply(logp, tf.stop_gradient(actions)), [-2, -1])
    pg_loss_batch = logp_selected * tf.stop_gradient(scores)
    if self._num_samples <= 0:
      pg_loss = tf.reduce_mean(
          tf.reduce_sum(pg_loss_batch * tf.stop_gradient(action_probs), 0))
    else:
      pg_loss = tf.reduce_mean(pg_loss_batch)
    if self._optimizer is None:
      self._optimizer = util.get_optimizer(optimizer)
      self._optimizer = self._optimizer(learning_rate=learning_rate)

    # entropy regularization.
    p_entropy = -tf.reduce_mean(tf.reduce_sum(self._p * self._logp, -1))
    tf.summary.scalar("p_entropy", p_entropy)
    # pg_loss += -p_entropy * 0.  # DEBUG/TODO: keep or not?

    tf.summary.scalar("pg_loss", pg_loss)
    train_op = tf.contrib.layers.optimize_loss(
        loss=pg_loss,
        global_step=tf.train.get_or_create_global_step(),
        learning_rate=learning_rate,
        variables=theta_params,
        optimizer=self._optimizer,
        name="PolicyGradient")
    return train_op

  def _sample(self, log_theta, num_samples=1, epsilon=0., greedy=False):
    """Sample actions for log_theta whose last dimension is multinomials.

    Args:
      epsilon: float control how much percent simply output uniformly.
      greedy: whether to sample from categorical or simply output its max.

    Remarks:
      It should also work if batch_size is a *list instead of a number.
    """
    actions = []
    log_theta_uniform = tf.zeros_like(log_theta)
    mask = tf.random_uniform(tf.shape(log_theta)[:-1], 0, 1) < epsilon
    mask = tf.cast(tf.expand_dims(mask, -1), tf.float32)
    log_theta_masked = log_theta_uniform * mask + log_theta * (1. - mask)
    if epsilon > 0 and greedy:
      raise ValueError("only set greedy=True when epsilon=0.")
    for i in range(num_samples):
      if greedy == True:
        noises = 0.
      else:
        noises = gb.sample_gumbel(tf.shape(log_theta))
      actions_ = tf.argmax(log_theta_masked + noises, -1)
      actions_ = tf.expand_dims(actions_, 0)
      actions.append(actions_)
    if num_samples == 1:
      actions = actions[0]
    else:
      actions = tf.concat(actions, 0)
    return actions


class RebarRelax(object):
  def __init__(self, K, D, hparams, theta_params=None):
    self._K = K
    self._D = D
    self._hparams = hparams
    self._theta_params = theta_params
    self._optimizer = None
    self._optimizer_v = None

    with tf.variable_scope("RelaxVars", reuse=False):
      initz1 = tf.ones_initializer
      self._tau = tf.get_variable("tau", [], initializer=initz1)
      self._eta = tf.get_variable("eta", [], initializer=initz1)
      self._phi_params = [self._eta]  # DEBUG: self._tau ill optimized.
      # self._phi_params = [self._tau, self._eta]  # CV params.

  def forward(self, logits, action_prob=True, is_training=False):
    """
    Args:
      action_prob: if True, also return one-hot vector weighted by probability,
        i.e. 0 still zero, but non-zero weighted by probability.

    Returns:
      (3, batch_size, D, K) where each of D-dimension is one-hot.

    Remarks:
      It should also work if batch_size is a *list instead of a number.
    """
    p = tf.nn.softmax(logits)  # Probability.
    logp = safer_log(p)
    b, z_softmax, zb_softmax, z, zb = gb.gumbel_softmax(
        p,
        temperature=self._tau,
        random=True,
        straight_through=False,
        logits_are_probas=True,
        return_raw_z=True,
        is_training=is_training)
    actions = tf.stack([b, z_softmax, zb_softmax], 0)
    action_probs_onehot = actions * p

    if is_training:
      self._p = p
      self._logp = logp
      self._b = b
      self._z = z
      self._zb = zb
      self._z_softmax = z_softmax
      self._zb_softmax = zb_softmax
      self._actions = actions
      self._action_probs_onehot = action_probs_onehot

    if action_prob:
      return actions, action_probs_onehot
    else:
      return actions

  def backward(self, scores, theta_params=None):
    """When *training*, call this to get the train_op for the policy.

    Args:
      scores: f(b) evaluation of size (num_samples * 3, batch_size), where
        num_samples should be 1.

    Returns:
      Training ops.
    """
    optimizer = self._hparams.policy_optimizer
    learning_rate = self._hparams.policy_learning_rate
    learning_var_rate = self._hparams.policy_var_learning_rate
    control_variate = self._hparams.policy_control_variate
    if theta_params is None:
      theta_params = self._theta_params
    sanity_check_for_unbiased_estimator = False   # CAREFUL with this setting.
    if sanity_check_for_unbiased_estimator:
      sanity_optimizing_step = 000
      optimzing_phase = tf.less(
          tf.train.get_or_create_global_step(), sanity_optimizing_step)
      learning_rate *= tf.cast(optimzing_phase, tf.float32)
      learning_var_rate *= tf.cast(optimzing_phase, tf.float32)
      tf.summary.scalar("learning_rate_relax", learning_rate)
      tf.summary.scalar("learning_var_rate_relax", learning_var_rate)

    # Policy gradient loss with baseline.
    scores_b, scores_z, scores_zb = tf.split(scores, 3, axis=0)
    batch_size_shape = tf.shape(scores)[1:]  # (batch_size, )
    scores_b, scores_z, scores_zb = (
        tf.reshape(scores_b, batch_size_shape),
        tf.reshape(scores_z, batch_size_shape),
        tf.reshape(scores_zb, batch_size_shape))
    cphi_z, cphi_zb = self._eta * scores_z, self._eta * scores_zb
    cphi_zb_sg = self._eta * tf.stop_gradient(scores_zb)
    if control_variate and (self._hparams.policy_var_learning_rate > 0. or (
        sanity_check_for_unbiased_estimator)):
      with tf.variable_scope("RelaxVars", reuse=tf.AUTO_REUSE) as vs:
        D = tf.make_template("z_cv", tf.layers.dense)
        _feat_dims = np.product(self._z.shape.as_list()[-2:])  # D * K.
        _dense_in_shape = tf.concat(  # (batch_size, D * K)
            [batch_size_shape, tf.constant([_feat_dims], dtype=tf.int32)], 0)
        r_z = tf.reshape(
            D(tf.reshape(self._z, _dense_in_shape), 1),
            batch_size_shape)
        r_zb = tf.reshape(
            D(tf.reshape(self._zb, _dense_in_shape), 1),
            batch_size_shape)
        r_zb_sg = tf.reshape(
            D(tf.reshape(tf.stop_gradient(self._zb), _dense_in_shape), 1),
            batch_size_shape)
        tf.summary.scalar("r_z", tf.reduce_mean(r_z))
        tf.summary.scalar("r_zb", tf.reduce_mean(r_zb))
        tf.summary.scalar("r_zb_sg", tf.reduce_mean(r_zb_sg))
        cphi_z += r_z
        cphi_zb += r_zb
        cphi_zb_sg += r_zb_sg
        self._new_phi = tf.get_collection(
            tf.GraphKeys.TRAINABLE_VARIABLES, scope=vs.name + "/z_cv")
        if len(self._new_phi) == 0:
          raise ValueError("failed to obtain new_phi. check scope.")
        self._phi_params += self._new_phi
    else:
      r_z = r_zb = r_zb_sg = tf.constant(0.)
    logp_selected = tf.reduce_sum(  # prod(p) == sum(logp) forall each in D.
        tf.multiply(self._logp, tf.stop_gradient(self._b)), [-2, -1]) # (batch_size,)
    scores_pg_t = tf.stop_gradient(scores_b - cphi_zb)  # For policy params theta.
    scores_pg_p = tf.stop_gradient(scores_b) - cphi_zb_sg  # For variance params phi.
    relax_loss_t = tf.reduce_mean(logp_selected*scores_pg_t + cphi_z - cphi_zb)
    # relax_loss_p = tf.reduce_mean(logp_selected*scores_pg_p + cphi_z - cphi_zb)
    # DEBUG/TODO: the following surrogate is incorrect.
    # Variance reduction only for new_phi (avoid 2nd grad of f function ops).
    relax_loss_p = tf.reduce_mean(logp_selected*(-r_zb_sg) + r_z - r_zb)

    # Policy variance loss.
    relax_gvar_loss = []
    for grad_theta in tf.gradients(relax_loss_p, theta_params):
      relax_gvar_loss.append(tf.reduce_mean(tf.square(grad_theta)))
    relax_gvar_loss = tf.reduce_mean(relax_gvar_loss)
    """ Another way of averaging square(g).
    relax_gvar_loss = []
    for grad_theta in tf.gradients(relax_loss_p, theta_params):
      relax_gvar_loss.append(tf.reshape(grad_theta, [-1]))
    grad_long = tf.concat(relax_gvar_loss, 0)
    relax_gvar_loss = tf.reduce_mean(tf.square(grad_long))
    """

    # Optimizer and train ops.
    if self._optimizer is None:
      self._optimizer = util.get_optimizer(optimizer)
      self._optimizer = self._optimizer(learning_rate=learning_rate)
    train_op_theta = tf.contrib.layers.optimize_loss(
        loss=relax_loss_t,
        global_step=tf.train.get_or_create_global_step(),
        learning_rate=learning_rate,
        variables=theta_params,
        optimizer=self._optimizer,
        name="RelaxTheta")
    if self._optimizer_v is None:
      self._optimizer_v = util.get_optimizer(optimizer)
      self._optimizer_v = self._optimizer_v(learning_rate=learning_var_rate)
    if self._hparams.policy_var_learning_rate > 0.:
      train_op_phi = tf.contrib.layers.optimize_loss(
          loss=relax_gvar_loss,
          global_step=tf.train.get_or_create_global_step(),
          learning_rate=learning_var_rate,
          variables=self._phi_params,
          optimizer=self._optimizer_v,
          name="RelaxPhi")
      train_op = tf.group(train_op_theta, train_op_phi)
    else:
      train_op = train_op_theta
    tf.summary.scalar("tau", self._tau)
    tf.summary.scalar("eta", self._eta)

    # --------- Sanity check for the unbiased estimator (BEGIN) --------- #
    def accumulate_grad(loss=None, grad_long=None,
                        name="sanity", opt_var_params=None):
      """Only one of the loss and grad_long is not None"""
      if loss is not None:
        if isinstance(loss, tuple) or isinstance(loss, list):
          loss_t, loss_p = loss
        else:
          loss_t = loss_p = loss
        relax_gvar_loss = []
        for grad_theta in tf.gradients(loss_t, theta_params):
          relax_gvar_loss.append(tf.reshape(grad_theta, [-1]))
        grad_long = tf.concat(relax_gvar_loss, 0)

      with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        if opt_var_params:
          assert loss is not None
          relax_gvar_loss = []
          for grad_theta in tf.gradients(loss_p, theta_params):
            relax_gvar_loss.append(tf.reduce_mean(grad_theta**2))
          relax_gvar_loss = tf.reduce_mean(relax_gvar_loss)
          lr_ = 1e-2
          train_op_phi = tf.contrib.layers.optimize_loss(
              loss=relax_gvar_loss,
              global_step=tf.train.get_or_create_global_step(),
              learning_rate=lr_,
              variables=opt_var_params,
              optimizer=util.get_optimizer("adam")(learning_rate=lr_),
              name="relaxphi")
          # train_op_phi = tf.assign_add(self._tau, -self._tau + 1.)
          self._tau = tf.Print(
              self._tau, [self._tau, self._eta, relax_gvar_loss])
          tf.summary.scalar("tau", self._tau)
          tf.summary.scalar("eta", self._eta)

        num_variables = int(1e7)
        assert_op = tf.Assert(
            tf.less_equal(tf.shape(grad_long)[0], num_variables),
            [tf.shape(grad_long), num_variables])
        with tf.control_dependencies([assert_op]):
          grad_sum = tf.get_variable(
              name + "_acc_grad",
              shape=(num_variables,),
              initializer=tf.zeros_initializer)
          grad_sum = tf.assign_add(
              grad_sum, tf.concat(
                  [grad_long, 
                  tf.zeros([num_variables - tf.shape(grad_long)[0]])],
                  0))
          grad_sum_t = grad_sum[:tf.shape(grad_long)[0]]
          # Note: average after square to make divergence appears faster.
          grad_mean_scale = tf.reduce_mean(tf.square(grad_sum_t) / tf.cast(
              tf.train.get_or_create_global_step(), tf.float32))
          tf.summary.scalar(name + "_acc_grad_mean", grad_mean_scale)
          grad_var_scale = tf.reduce_mean(tf.square(grad_long))
          tf.summary.scalar(name + "_acc_grad_var_appox", grad_var_scale)
        if opt_var_params:
          op = tf.group(grad_sum, train_op_phi)
        else:
          op = grad_sum
        return op
    if sanity_check_for_unbiased_estimator:
      # Specify the loss for gradient checker.
      _relax_loss_t = tf.reduce_mean(
          -logp_selected * tf.stop_gradient(cphi_zb) + cphi_z - cphi_zb)
      _relax_loss_p = tf.reduce_mean(
          -logp_selected * cphi_zb_sg + cphi_z - cphi_zb)
      main_loss = (_relax_loss_t, _relax_loss_p)  # expect zero mean.
      #main_loss = (relax_loss_t, relax_loss_p)  # expect non-zero mean.
      ref_loss = -tf.reduce_mean(cphi_z)
      ref_loss2 = tf.reduce_mean(cphi_zb)
      op1 = accumulate_grad(main_loss, name="sanity",
                            #opt_var_params=self._new_phi)
                            #opt_var_params=[self._tau])
                            opt_var_params=self._phi_params)
      op2 = accumulate_grad(ref_loss, name="sanity_ref")
      op3 = accumulate_grad(ref_loss2, name="sanity_ref2")
      train_op = tf.cond(optimzing_phase,
                         lambda: tf.group(train_op, train_op),
                         lambda: tf.group(train_op, op1, op2, op3))
    # --------- Sanity check for the unbiased estimator (END) --------- #

    return train_op