import tensorflow as tf


def enn(embs, D, d, outd, is_training):
  """Generate embedding from the code. (M, D, d) -> (M, d).
  """

  # stop gradients for 0:ith embs:
  """
  """
  first_k = 2
  embs = tf.unstack(embs, axis=1)
  embs_new = []
  for i, emb in enumerate(embs):
    emb = tf.expand_dims(emb, 1)
    if i < first_k:
      embs_new.append(tf.stop_gradient(emb))
    else:
      embs_new.append(emb)
  embs = tf.concat(embs_new, axis=1)

  """
  alpha = 1.0 - tf.train.inverse_time_decay(
      1.0,
      tf.train.get_or_create_global_step(),
      10000,
      1.,
      staircase=False)
  alpha = tf.clip_by_value(alpha, 0.0, 1.0)
  if is_training:
    tf.summary.scalar("alpha", alpha)
  """

  h = 0.
  loss_reg = 0.
  h_prev = None
  for i in range(1, D + 1):
    #global_step = tf.train.get_global_step()
    # w = tf.nn.relu(tf.cast(global_step, tf.float32) - tf.convert_to_tensor(3000. * (i-1)))
    h_ = tf.layers.dense(
        #tf.reduce_prod(embs[:, :i, :], axis=1),
        tf.reshape(embs[:, :i, :], [tf.shape(embs)[0], d * i]),
        #tf.reshape(embs[:, i-1, :], [tf.shape(embs)[0], d]),
        d,
        activation=tf.nn.relu,
        use_bias=True)
    h_ = tf.layers.dense(h_, outd, use_bias=True)
    #if i == 2:
    #  h_ *= alpha
    #h += h_
    #h += 0.5**(i-1) * h_  # TODO: more principle.
    #h += w * h_
    #h += tf.cast(i == 1, tf.float32) * h_
    #h += tf.cast(i == 2, tf.float32) * h_
    if i < first_k + 1:
      h_ = tf.stop_gradient(h_)
    h += tf.cast(i <= first_k + 1, tf.float32) * h_
    h_ = tf.reduce_mean(h_**2)
    if h_prev is not None:
      loss_reg += 10. * tf.nn.relu(h_ - h_prev)
    if is_training:
      tf.summary.scalar("h_%d"%i, tf.reduce_mean(h_**2))
    h_prev = h_
  tf.add_to_collection(tf.GraphKeys.REGULARIZATION_LOSSES, loss_reg)

  return h