"""Generating synthetic data."""

import os
import numpy as np
import cPickle as pickle
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D as ax3d
import seaborn as sns

import sklearn
from sklearn import datasets as ds
import tensorflow as tf

home = os.path.expanduser("~")


class EmbeddingData(object):
  """Class for generating embedding data for discrete set of symbols."""

  def __init__(self,
               vocab_size,
               method,
               emb_dim=None,
               num_clusters=None,
               cluster_std=None,
               seed=42):
    legit_methods = ["random", "clusters", "hi_clusters", "hi_clusters_sym",
                     "two_circles" "two_moons",  "s_curve", "swiss_roll",
                     "glove.6B.50d", "glove.6B.100d", "glove.6B.200d",
                     "glove.6B.300d"]

    if method not in legit_methods and not method.startswith("pretrained"):
      raise ValueError("method {} is not legit".format(method))
    if method in ["random"] and emb_dim is None:
      raise ValueError(
          "emb_dim needs to be specified when using method {}".format(method))
    if method in ["clusters", "hi_clusters", "hi_clusters_sym"] and (
        num_clusters is None or cluster_std is None):
      raise ValueError(
          "num_clusters and cluster_std needs to be specified "
          "when using method {}".format(method))

    self._v = vocab_size
    self._d = emb_dim
    self._method = method
    self._c = num_clusters
    self._c_std = cluster_std
    self._seed = seed

    with tf.device("/cpu:0"):
      self._data_gen()

  def _data_gen(self):
    np.random.seed(self._seed)
    method = self._method
    if method == "random":
      init = np.random.random(self._v * self._d) * 10
      init = init.reshape([self._v, self._d])
      self._y = None
    elif method == "clusters":
      init, self._y = ds.make_blobs(self._v,
                                    n_features=self._d,
                                    centers=self._c,
                                    cluster_std=self._c_std)
    elif method == "hi_clusters":
      # Two level hierarchical clusters.
      init, self._y = ds.make_blobs(self._v,
                                    n_features=self._d,
                                    centers=self._c,
                                    cluster_std=self._c_std,
                                    shuffle=True)
      # Partition the clusters into sqrt numbers, and each sequentially belongs
      # to a higher level cluster by translation.
      hi_cluster_size = int(np.sqrt(self._c))
      hi_centers, _ = ds.make_blobs(hi_cluster_size,
                                    n_features=self._d,
                                    centers=hi_cluster_size,
                                    cluster_std=self._c_std,
                                    shuffle=True)

      y2hi_center = {}
      for i in range(hi_cluster_size):
        hi_center = hi_centers[i] * 5
        if i == hi_cluster_size - 1:
          for j in range(i * hi_cluster_size, self._c):
            y2hi_center[j] = hi_center
        else:
          for j in range(i * hi_cluster_size, (i + 1) * hi_cluster_size):
            y2hi_center[j] = hi_center
      for i, y in enumerate(self._y):
        init[i] += y2hi_center[y]
    elif method == "hi_clusters_sym":
      # Two level hierarchical clusters, each bigger clusters are symmetric.
      hi_cluster_size = int(np.sqrt(self._c))  # branching factor in the tree.
      assert hi_cluster_size**2 == self._c
      _num_unique_nodes = self._v // hi_cluster_size
      assert hi_cluster_size * _num_unique_nodes == self._v
      _init, _y = ds.make_blobs(_num_unique_nodes,
                                    n_features=self._d,
                                    centers=hi_cluster_size,
                                    cluster_std=self._c_std,
                                    shuffle=True)
      # Copy the points to  different locations.
      hi_centers, _ = ds.make_blobs(hi_cluster_size,
                                    n_features=self._d,
                                    centers=hi_cluster_size,
                                    cluster_std=self._c_std,
                                    shuffle=True)
      hi_centers = [hi_center * 5 for hi_center in hi_centers]
      inits = [_init.copy() for _ in range(hi_cluster_size)]
      ys = [_y.copy() + hi_cluster_size * i for i in range(hi_cluster_size)]
      for i in range(hi_cluster_size):
        for j in range(_num_unique_nodes):
          inits[i][j] += hi_centers[i]
      init = np.vstack(inits)
      self._y = np.hstack(ys)
    elif method == "two_circles":
      init, self._y = ds.make_circles(self._v, shuffle=True, noise=.02)
      self._d = 2
    elif method == "two_moons":
      init, self._y = ds.make_moons(self._v, shuffle=True, noise=0.05)
      self._d = 2
    elif method == "s_curve":
      init, self._y = ds.make_s_curve(self._v, noise=0.)
      self._d = 3
    elif method == "swiss_roll":
      init, self._y = ds.make_swiss_roll(self._v, noise=0.)
      self._d = 3
    elif method.startswith("glove"):
      file_path = home + "/dbase/parameters/glove.6B/" + method + '.pkl'
      with open(file_path) as fp:
        data = pickle.load(fp)
      vocab, init = data["vocab"], data["embs"]
      if self._v is None:
        self._v = len(vocab)
      self._y = vocab[:self._v]
      init = init[:self._v]
      self._d = int(method.split('.')[-1][:-1])
    elif method.startswith("pretrained_embs"):
      # This is similar as glove, but it require the emb file saved in
      # working folder with name `$method.pkl`.
      file_path = "%s.pkl" % method
      with open(file_path) as fp:
        data = pickle.load(fp)
      vocab, init = data["vocab"], data["embs"]
      if self._v is None:
        self._v = len(vocab)
      self._y = vocab[:self._v]
      init = init[:self._v]
      self._d = init.shape[1]
    elif method.startswith("pretrained"):
      # This is similar as glove, but it require the emb file saved in
      # working folder with name 
      _, subfolder, emb_file = method.split("--")
      source_folder = home + "/pbase/x/p704/pretrains/" + subfolder + "/"
      file_path = source_folder + emb_file
      self.output_code_path = source_folder
      if emb_file.endswith(".pkl"):
        self.output_code_path += emb_file[:-4] + ".code."
      else:
        self.output_code_path += emb_file + ".code."
      with open(file_path) as fp:
        init = pickle.load(fp)
      if self._v is None:
        self._v = init.shape[0]
      self._d = init.shape[1]
      self._y = range(self._v)
    else:
      raise ValueError("method {} is not legit".format(method))

    self._data_emb = tf.get_variable("data_emb",
                                     initializer=init.astype('float32'),
                                     trainable=False)

  def plot(self, data_emb=None):
    """Plot data_emb."""
    if data_emb is None:
      with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        data_emb = sess.run(self._data_emb)
    if data_emb.shape[1] == 2:
      plt.scatter(data_emb[:, 0], data_emb[:, 1])
      plt.show()
    elif data_emb.shape[1] == 3:
      fig = plt.figure()
      ax = fig.add_subplot(111, projection='3d')
      ax.scatter(data_emb[:, 0], data_emb[:, 1], data_emb[:, 2])
      plt.show()
    else:
      raise ValueError(
          "Cannot visualize when dimension is not 2 or 3. Now is {}".format(
          data_emb.shape[1]))


  def batch(self, batch_size, repeat=None):
    """Returns a batch of x and y."""
    with tf.device("/cpu:0"):
      idxs_data = tf.data.Dataset.range(self._v)
      idxs_data = idxs_data.shuffle(buffer_size=self._v)
      idxs_data = idxs_data.batch(batch_size).repeat(repeat)
      idxs_iterator = idxs_data.make_one_shot_iterator()
      idxs = idxs_iterator.get_next()
      embs = tf.nn.embedding_lookup(self._data_emb, idxs)
    return (idxs, embs)

  @property
  def data_emb(self):
    return self._data_emb

  @property
  def y(self):
    """Data point indicator, e.g. labels, cluster assignments, coordinates."""
    return self._y

  @property
  def d(self):
    return self._d

  @property
  def vocab_size(self):
    return self._v


def plot_data(X, filename=None):
  ''' X: N * d where d has to be 2-dimensional.
  '''
  fig = plt.figure()
  if len(X.shape) == 2 and X.shape[1] != 2:
    raise ValueError("only 2d data can be plotted.")
  plt.scatter(X[:, 0], X[:, 1])
  if filename:
    plt.savefig(filename + ".png", bbox_inches="tight")
  plt.close()
