import numpy as np
import tensorflow as tf

import sys
sys.path.insert(0, "../../lm")

import util


def X2Z(X,
        encoder,
        hparams,
        encode=False,
        is_training=True,
        scope="X2Z"):
  """First X->H, then H->Z, where Z is the discrete code samples.

  Args:
    X: data ids of (batch_size, ) when encode is False, otherwise they are
      input data features of (batch_size, d).
    encode: if True, H = f(X), otherwise H = lookup(X).

  Returns:
    codes: (batch_size, D, K)
  """
  with tf.variable_scope(scope, reuse=not is_training):
    if encode:  # X -> codes
      num_layers = hparams.ec_emb_transpose_layers
      h_dim = hparams.ec_emb_transpose_dim
      h_actv = util.get_activation(hparams.ec_emb_transpose_actv)
      K, D = hparams.K, hparams.D
      out_dim = D * K

      H = X
      for l in range(num_layers):
        if l != num_layers - 1:
          H = h_actv(tf.layers.dense(H, h_dim))
        else:
          H = tf.layers.dense(H, out_dim)
          H = tf.reshape(H, [-1, D, K])
      code_logits = H

      codes, _ = encoder.symbol2code(
          None, logits=code_logits, is_training=is_training)
    else:  # idxs -> codes
      codes, _ = encoder.symbol2code(
          X, logits=None, is_training=is_training)

  return codes


def Z2X(codes,
        encoder,
        hparams,
        is_one_hot=True,
        is_training=True,
        scope="Z2X"):
  with tf.variable_scope(scope, reuse=not is_training):
    embs = encoder.embed(
        codes, is_one_hot=is_one_hot, is_training=is_training)
  return embs
