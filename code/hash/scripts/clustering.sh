#!/bin/bash
cd ..

gpu=$1
num_steps=10000
output_iters=1000
data_gen_method=clusters
vocab_size=10000
num_clusters=100
cluster_std=0.1
batch_size=1024
code_save_filename=
code_load_filename=

K=$num_clusters
D=1
learning_rate=1e-3
ec_STE_softmax_transform=True
ec_hard_code_output=True
ec_code_generator=gumbel_softmax
ec_code_generator=STE_argmax
#ec_code_generator=preassign
ec_logits_bn=1.
ec_entropy_reg=0.0
#ec_temperature_decay_method=none
#ec_temperature_decay_method=inv_time
ec_temperature_decay_steps=100
ec_temperature_decay_rate=1
ec_temperature_init=1
ec_temperature_lower_bound=1e-1

ec_emb_transpose_layers=2
ec_emb_transpose_dim=200
ec_emb_transpose_actv=relu
ec_aggregator=mean
#ec_aggregator=rnn
ec_shared_coding=False
ec_code_emb_dim=200
ec_rnn_num_layers=1
ec_rnn_bidirection=False
ec_rnn_hidden_size=200

#for emb_dim in 2 ; do
for emb_dim in 2 10; do
#for use_x2z_encode in False; do
for use_x2z_encode in True False ; do
#for ec_temperature_decay_method in inv_time; do
for ec_temperature_decay_method in none inv_time; do
	save_path=~/pbase/x/p704/results/temp/$(date +"%m%d_%H%M%S")\_dim$emb_dim\_encode$use_x2z_encode\_temp$ec_temperature_decay_method
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python hash_main.py --data_gen_method=$data_gen_method --output_iters=$output_iters --batch_size=$batch_size --vocab_size=$vocab_size --emb_dim=$emb_dim --num_clusters=$num_clusters --cluster_std=$cluster_std --save_path=$save_path --code_save_filename=$code_save_filename --code_load_filename=$code_load_filename --num_steps=$num_steps --use_x2z_encode=$use_x2z_encode --learning_rate=$learning_rate --K=$K --D=$D --ec_rnn_num_layers=$ec_rnn_num_layers --ec_rnn_bidirection=$ec_rnn_bidirection --ec_code_generator=$ec_code_generator --ec_STE_softmax_transform=$ec_STE_softmax_transform --ec_hard_code_output=$ec_hard_code_output --ec_aggregator=$ec_aggregator --ec_code_emb_dim=$ec_code_emb_dim --ec_rnn_hidden_size=$ec_rnn_hidden_size --ec_shared_coding=$ec_shared_coding --ec_temperature_decay_method=$ec_temperature_decay_method --ec_temperature_decay_steps=$ec_temperature_decay_steps --ec_temperature_decay_rate=$ec_temperature_decay_rate --ec_temperature_init=$ec_temperature_init --ec_temperature_lower_bound=$ec_temperature_lower_bound --ec_logits_bn=$ec_logits_bn --ec_entropy_reg=$ec_entropy_reg --ec_emb_transpose_layers=$ec_emb_transpose_layers --ec_emb_transpose_dim=$ec_emb_transpose_dim -ec_emb_transpose_actv=$ec_emb_transpose_actv >$save_path/log 2>&1
done
done
done
