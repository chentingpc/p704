#!/bin/bash
cd ..

data_gen_method=pretrained--lm--large_embs.pkl
data_gen_method=pretrained--lm--medium_embs.pkl
data_gen_method=pretrained--lm--small_embs.pkl

code_save_filename=code_name_holder
save_root=~/pbase/x/p704/results/temp/
num_steps=30000
gpu=$1
output_iters=3000
batch_size=1024
learning_rate=1e-3
ec_STE_softmax_transform=True
ec_hard_code_output=True
#ec_code_generator=preassign
#ec_code_generator=gumbel_softmax
ec_code_generator=STE_argmax
ec_logits_bn=1.

for KD in 16,8 32,8 64,8 16,16 32,16 64,16 16,32 32,32, 64,32; do
IFS=","; set -- $KD; K=$1; D=$2
for dim in 200; do
for compound in none,100,1,1,1e-1 ; do
#for compound in inv_time,1000,1,1,1e-1 ; do
IFS=","; set -- $compound; method=$1; step=$2; rate=$3; ec_temperature_init=$4; ec_temperature_lower_bound=$5;
for ec_aggregator in mean ; do
#for ec_aggregator in mean rnn mean_fnn; do
for use_x2z_encode in True; do
#for use_x2z_encode in True False; do
for ec_entropy_reg in 0.0; do
	ec_emb_transpose_layers=1
	ec_emb_transpose_dim=200
	ec_emb_transpose_actv=relu
	ec_shared_coding=False
	ec_fnn_hidden_size=200
	ec_rnn_num_layers=1
	ec_rnn_bidirection=False
	ec_temperature_decay_method=$method
	ec_temperature_decay_steps=$step
	ec_temperature_decay_rate=$rate
	ec_code_emb_dim=$dim
	ec_rnn_hidden_size=$dim

	save_path=$save_root/$(date +"%m%d_%H%M%S")\_$data_gen_method\_K$K\_D$D\_$ec_code_generator\_x2z$use_x2z_encode\_hsize$dim\_hard$ec_hard_code_output\_decay$method\_step$step\_rate$rate\_init$ec_temperature_init\_low$ec_temperature_lower_bound\_aggregator$ec_aggregator
	echo $save_path
	if [ ! -e $save_path ]; then
		mkdir -p $save_path
	fi
	CUDA_VISIBLE_DEVICES=$gpu stdbuf -oL -eL python hash_main.py --data_gen_method=$data_gen_method --output_iters=$output_iters --batch_size=$batch_size --save_path=$save_path --code_save_filename=$code_save_filename --num_steps=$num_steps --use_x2z_encode=$use_x2z_encode --learning_rate=$learning_rate --K=$K --D=$D --ec_rnn_num_layers=$ec_rnn_num_layers --ec_rnn_bidirection=$ec_rnn_bidirection --ec_code_generator=$ec_code_generator --ec_STE_softmax_transform=$ec_STE_softmax_transform --ec_hard_code_output=$ec_hard_code_output --ec_aggregator=$ec_aggregator --ec_code_emb_dim=$ec_code_emb_dim --ec_rnn_hidden_size=$ec_rnn_hidden_size --ec_shared_coding=$ec_shared_coding --ec_temperature_decay_method=$ec_temperature_decay_method --ec_temperature_decay_steps=$ec_temperature_decay_steps --ec_temperature_decay_rate=$ec_temperature_decay_rate --ec_temperature_init=$ec_temperature_init --ec_temperature_lower_bound=$ec_temperature_lower_bound --ec_logits_bn=$ec_logits_bn --ec_entropy_reg=$ec_entropy_reg --ec_emb_transpose_layers=$ec_emb_transpose_layers --ec_emb_transpose_dim=$ec_emb_transpose_dim --ec_emb_transpose_actv=$ec_emb_transpose_actv --ec_fnn_hidden_size=$ec_fnn_hidden_size >$save_path/log 2>&1
done
done
done
done
done
done
